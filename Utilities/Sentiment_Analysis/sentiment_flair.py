import pandas as pd
import flair
from flair.models import TextClassifier

sentiment_model = TextClassifier.load('en-sentiment')

def differentSentiments(text1, text2, threshold=0.4):
    sample1 = flair.data.Sentence(text1)
    sample2 = flair.data.Sentence(text2)

    # Predict on the text passages
    sentiment_model.predict(sample1)
    sentiment_model.predict(sample2)

    # Get the sentiments (Positive / Negative)
    sent1 = sample1.labels[0].value
    sent2 = sample2.labels[0].value

    # Get the confidence scores
    conf1 = sample1.labels[0].score
    conf2 = sample2.labels[0].score

    print(sent1 + " - " + str(conf1))
    print(sent2 + " - " + str(conf2))

    if (sent1 != sent2) and (conf1*conf2 > threshold):
        return True

    return False


def querying():
    while True:
        sen1 = input("\nPlease enter a sentence (Or 'q' to quit): ")
        if sen1=="q" or sen1=="Q":
            break

        sen2 = input("\nPlease enter a second sentence: ")

        result = differentSentiments(sen1, sen2)
        print(result)

querying()