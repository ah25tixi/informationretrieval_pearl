from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


class SentimentAnalyzer():
    def __init__(self, query=""):
        """
        :param query: The text of the query against which to compare subsequent texts
        """

        # Initialize the analyzer and get the sentiments of the compareText for later reference
        self.Analyzer = SentimentIntensityAnalyzer()
        self.sentiment = self.Analyzer.polarity_scores(query)

    # Function to update the query
    def setQueryText(self, query):
        self.sentiment = self.Analyzer.polarity_scores(query)

    def updateStanceBySentiment(self, discussion_title, arg_stance):
        # Get the sentiment of the title
        sentiment = self.Analyzer.polarity_scores(discussion_title)

        # Multiply both compound score (will be negative in case of opposing sentiments)
        signMult = sentiment["compound"]*self.sentiment["compound"]

        # If the sentiments differ, provide and updated stance
        if signMult < 0:
            if arg_stance=="PRO":
                return "CON"
            return "PRO"

        return arg_stance

    def setStanceArgDataframe(self, df):
        for i, row in df.iterrows():
            # Get the premises of an argument and score them
            premises = row["premises"]
            self.setQueryText(premises)

            # If a polarity is detected, change the stance
            if self.sentiment["compound"] > 0:
                row["stance"] = "PRO"
            elif self.sentiment["compound"] < 0:
                row["stance"] = "CON"

        # Return the updated dataframe
        return df

    def getStanceString(self, string, stance):
        self.setQueryText(string)
        if self.sentiment["compound"] > 0:
            return "PRO"
        elif self.sentiment["compound"] < 0:
            return "CON"
        else:
            return stance






def testSentiments(text1, text2):
    sidObj = SentimentIntensityAnalyzer()

    sen_dict1 = sidObj.polarity_scores(text1)
    sen_dict2 = sidObj.polarity_scores(text2)

    print(sen_dict1)
    print(sen_dict2)

    signMult = sen_dict1["compound"] * sen_dict2["compound"]

    # If the compound values have different signs, return that the sentiments are different
    if signMult < 0:
        return True

    return False


