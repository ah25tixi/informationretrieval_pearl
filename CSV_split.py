import Utilities.sentence_split as spl

import pandas as pd
from io import StringIO

# Splits are large CSV file into smaller parts of [chunksize] rows
def row_split(in_path, out_path):
    rows = pd.read_csv(in_path, chunksize=10000, iterator=True)

    for i, chunk in enumerate(rows, 1):
        print("Reading chunk ", i)
        chunk.to_csv(out_path + 'args_processed_part_{}.csv'.format(i))


# Tailored function to split the original CSV into columns for later processing
def args_split(in_path, out_path):
    with open(in_path) as inf:
        with open(out_path, 'w') as outf:
            for i, line in enumerate(inf):
                print("Updating columns for argument %d" % i)

                if(i==0):
                    #Important Note: This format is used for the original CSV;
                    # Files split by the "row_split" function need the format ",id,conclusion,..." (Comma before id)
                    new_line = 'id,conclusion,premises,stance,context1,discussion_title,context2\n'

                else:
                    new_line = rowPreparationArgs(line)

                outf.write(new_line)


# Tailored function to split the CSV processed by args_split into columns for later processing
def sentence_split(in_path, out_path, min_len=80):
    """
    param: min_len - The minimal length in characters for a sentence to be saved
    """

    with open(in_path) as inf:
        with open(out_path, 'w') as outf:
            for i, line in enumerate(inf):
                print("Splitting argument %d into sentences" % i)

                if (i == 0):
                    # Create new header
                    outf.write('args_id,sen_id,sentence,conclusion,stance,quality\n')

                else:
                    writeSentences(line, outf, min_len)




# ===========================================================
# Helper functions
# ===========================================================

def rowPreparationArgs(line):
    # Read in the line as Dataframe to access the columns
    header = "id,conclusion,premises,context,sentences\n"
    data = StringIO(header + line)
    lineDF = pd.read_csv(data, sep=",")

    # Get the relevant parts + Remove quotation marks (to avoid unwanted splits)
    id = lineDF.iloc[0, 0]
    conclusion = lineDF.iloc[0, 1].replace('"', "'")
    premises = lineDF.iloc[0, 2].replace('"', "'")
    context = lineDF.iloc[0, 3].replace('"', "'")

    # Process the premises
    # - Remove annotations
    # - Split premises from stances
    premises = premises.replace("', 'annotations': []}]", '')
    premises = premises.replace("[{'text': '", '').replace("', 'stance': '", '",')

    # Process the context
    context = context.replace("'discussionTitle': '", '","').replace("', 'mode':", '","')

    # Create new line
    new_line = id + ',"' + conclusion + '","' + premises + ',"' + context + '"\n'

    return new_line



def writeSentences(line, outfile, min_len):
    # Read in the line as Dataframe to access the columns
    header = "id,conclusion,premises,stance,discussion_title,quality\n"
    data = StringIO(header + line)
    lineDF = pd.read_csv(data, sep=",")

    # Get the argument id and the premises
    id = lineDF.iloc[0, 0]  # first row and first column
    conclusion = lineDF.iloc[0, 1] # first row and second column
    premises = lineDF.iloc[0, 2]
    stance = lineDF.iloc[0, 3]
    quality = lineDF.iloc[0, 5]


    # Split the premises into individual sentences
    sentences = spl.split_into_sentences(premises)

    # Save each sentence as new line with individual ID (based on the argument's ID and position of sentence)
    # - Sentences with length < min_len will be skipped
    for j, sen in enumerate(sentences):
        if len(sen) < min_len:
            continue

        sen_id = id + "_" + str(j)

        # Replace all in sentence quotation marks with annotation marks
        # -> Avoids unwanted splits
        sen = sen.replace('"', "'")

        new_line = id + ',' + sen_id + ',"' + sen + '","' + conclusion + '",' + stance + ',' + str(quality) + '\n'
        outfile.write(new_line)


