import pearl as p
import pandas as pd
from os.path import exists

#CONSTANTS
ARG_PATH = "./Indexing/args_corpus/argsArgRank_75_30.csv"
SEN_PATH = "./Indexing/args_corpus/sentences_slim.csv"
QUERIES = pd.read_csv("Evaluation/topics_2020_reduced.csv")['query']

# Basic retrieval
pearlRet = p.PearlRetriever(argsPath=ARG_PATH, senPath=SEN_PATH,
                            matching="neighbour",
                            argRank=False,
                            weightedScore=False,
                            numPairingCandidates=500)

pearlRet.retrieveToTxtTREC(queryset=QUERIES, filepath="run.txt", method="Blocklist")

# ArgRank-Retrieval
def argRankRetrieval(params):
    similarities = params.get("sim")
    alphas = params.get("alpha")
    weightings = params.get("cos")

    first_part = "./Data/ArgRanks/argsAR_discussion_minSim"

    for sim in similarities:
        sim = str(int(sim*100))
        print("\n" + "=" * 50)
        print("Obtaining ArgRank-Results for Cosine " + sim)
        print("=" * 50 + "\n")

        for alpha in alphas:
            alpha = str(int(alpha*100))

            for weight in weightings:
                if weight:
                    addition = "_CosWeighted"
                else:
                    addition = ""

                argsPath = first_part + sim + addition + "_groundRel_" + alpha + ".csv"



                outpath = "./Data/ArgRanks/Retrieval_Results/AR_Results_v3_minSim" \
                          + sim + addition + "_alpha" + alpha + ".csv"

                # Skip the retrieval if the argsfile does not exist or the outfile already exists
                if not exists(argsPath) or exists(outpath):
                    print("Infile does not exist or retrieval results were already obtained.")
                    print("Skipping the retrieval for " + argsPath + "\n")
                    continue

                retriever = p.PearlRetriever(argsPath=argsPath,
                                             senPath=SEN_PATH, matching="partner", argRank=True,
                                             numPairingCandidates=100)

                retriever.retrieveToCSV(QUERIES, filepath=outpath)


#min_sims = [0.75, 0.8, 0.85, 0.9]
#alphaValues = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
#cosWeighting = [False, True]
#params = {"sim": min_sims, "alpha": alphaValues, "cos": cosWeighting}

#argRankRetrieval(params)


# Indexing
#pearlInd = p.PearlIndexer(argsInPath=IN_PATH, argsOutPath=ARG_PATH, senPath=SEN_PATH, mode=1)
#pearlInd.indexArgs()
#pearlInd.indexSentences()

#Adding values
#pearlInd.addColumnsToArgs(quality_dir='./Quality/out/No Premise')
#args_dir='/media/zeb/TOSHIBA EXT/Data Science/Information Retrieval/Split_CSV',

# CSV Split
#pearlInd.argsSplit()
#pearlInd.sentencesSplit()



def querying(retriever):
    while True:
        query = input("\nPlease enter a query (Or 'q' to quit): ")
        if query=="q" or query=="Q":
            break

        retriever.retrieve(query)

        # Get top 10 sentences
        top10 = retriever.relevantSentences["sentence"].iloc[:10]
        for i, row in enumerate(top10):
            print(str(i+1) + ": " + row)
        print("==================================")





