import argparse
from time import sleep
import os
import tira_interface as ti

# Constants to define the method
# - No ArgRank-reranking is applied (Only blocklist and neighbour-matching)
# - Relevance scores from the argument and sentence retriever are weighted and added to create the final score

METHODNAME = "Blocklist_WeightedRelevance"
PARAMS = ti.METHODS.get(METHODNAME)

# Execution for Docker
if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input-dir")
    parser.add_argument("-o", "--output-dir")
    args = parser.parse_args()
    args = vars(args)

    # Give the containers time to reach a steady
    sleep(60)

    # Handle input
    input_dir = args['input_dir']
    output_dir = args['output_dir']

    # Make sure the output directory exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Create an instance of the tira interface with the specified method
    interface = ti.TIRAInterface(inputDir=input_dir, outputDir=output_dir)

    # Create the run
    interface.createRun(methodName=METHODNAME, params=PARAMS)