import sys
import pearl as p
from os.path import exists


# Constants
NUM_ARGS = 1000           # How many arguments the argsRetriever should retrieve
NUM_SEN = 5000            # How many sentences the senRetriever should retrieve
NUM_CANDIDATES = 500      # How many candidates should be used for pairing
NUM_PAIRS = 10            # How many results should be returned per query

METHODS = {"ArgRank8040_WeightedRelevance": {"argsPath": "argsArgRank_80_40.csv",
                                             "matching": "neighbour",
                                             "argRank": True,
                                             "weightedScore": True},

           "Blocklist_WeightedRelevance": {"argsPath": "argsArgRank_80_40.csv",
                                           "matching": "neighbour",
                                           "argRank": False,
                                           "weightedScore": True},

           "ArgRank8040": {"argsPath": "argsArgRank_80_40.csv",
                           "matching": "neighbour",
                           "argRank": True,
                           "weightedScore": False},

           "ArgRank7530": {"argsPath": "argsArgRank_75_30.csv",
                           "matching": "neighbour",
                           "argRank": True,
                           "weightedScore": False},

           "Blocklist": {"argsPath": "argsArgRank_80_40.csv",
                         "matching": "neighbour",
                         "argRank": False,
                         "weightedScore": False}}


class TIRAInterface:
    def __init__(self, inputDir, outputDir):
        # Define the directories
        self.senPath = "sentences_slim.csv"
        self.outpath = outputDir + "run.txt"
        self.topicPath = inputDir + "topics.xml"



    # Function that supervises the creation of the runs
    def createTIRASubmission(self):
        # Stop the program if a file already exists to avoid overwrites
        if exists(self.outpath):
           sys.exit("A run.txt-file already exists at the specified location. " + \
                 "Please delete or rename it before creating another submission.")

        # Start the submission process otherwise
        for methodName, params in METHODS.items():
            print("\n" + "="*50)
            print("Creating run for " + methodName)
            print("="*50 + "\n")

            self.createRun(methodName, params)


    # Function that takes a method name and corresponding parameters to create a TIRA-Run
    def createRun(self, methodName, params):
        # Create a new retriever based on the specifications
        retriever = p.PearlRetriever(argsPath=params.get("argsPath"),
                                     senPath=self.senPath,
                                     matching=params.get("matching"),
                                     argRank=params.get("argRank"),
                                     weightedScore=params.get("weightedScore"),
                                     numArgs=NUM_ARGS,
                                     numSen=NUM_SEN,
                                     numPairingCandidates=NUM_CANDIDATES,
                                     numPairs=NUM_PAIRS)

        # Create the run
        retriever.retrieveToTxtTREC(topicXML=self.topicPath,
                                    filepath=self.outpath,
                                    method=methodName)


