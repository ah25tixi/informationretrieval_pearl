import pandas as pd
import numpy as np
from transformers import AutoTokenizer
import torch
from torch import nn
from torch.utils.data import DataLoader
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from tqdm.auto import tqdm
from classes import CustomBERTModel, QualityDataset
import wandb

wandb.init(project="ArgumentQualityPrediction", entity="jonasp")
device = 'cuda'
out_name = "quality_prediction"

dataset = pd.read_csv('data/webis-argquality20-full.csv', usecols=['Premise', 'Combined Quality', 'Is Argument?'])  # , nrows=100)
dataset = dataset.loc[dataset['Is Argument?'], :]
text = dataset['Premise'].tolist()
scores = dataset['Combined Quality'].to_numpy().reshape(-1, 1)


# normalize scores
#    min_max_scaler = preprocessing.MinMaxScaler((0, 1))
#    scores = min_max_scaler.fit_transform(scores)

# Split in 80-10-10 Train-Val-Test Set
train_texts, val_texts, train_scores, val_scores = train_test_split(text, scores, test_size=.2)
val_texts, test_texts, val_scores, test_scores = train_test_split(val_texts, val_scores, test_size=.5)

# Tokenize text input
token_length = 512
tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", model_max_length=token_length)

train_encodings = tokenizer(train_texts, truncation=True, padding=True, return_tensors="pt")
val_encodings = tokenizer(val_texts, truncation=True, padding=True, return_tensors="pt")
test_encodings = tokenizer(test_texts, truncation=True, padding=True, return_tensors="pt")

train_dataset = QualityDataset(train_encodings, train_scores)
val_dataset = QualityDataset(val_encodings, val_scores)
test_dataset = QualityDataset(test_encodings, test_scores)

model = CustomBERTModel().to(device)

# Freeze pretrained bert layers
for param in model.bert.parameters():
    param.requires_grad = False

# Parameters from [Exploring BERT Synonyms and Quality Prediction
# for Argument Retrieval by Green, Moroldo, Valente]
batch_size = 16
learning_rate = 0.005
weight_decay = 0.0005
num_epochs = 30

train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)
eval_dataloader = DataLoader(val_dataset, batch_size=batch_size)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size)

criterion = nn.MSELoss()

optimizer = optim.AdamW(model.parameters(), lr=learning_rate, weight_decay=weight_decay)

num_training_steps = num_epochs * len(train_dataloader)
min_valid_loss = np.inf

wandb.config = {
    "learning_rate": learning_rate,
    "weight_decay": weight_decay,
    "epochs": num_epochs,
    "batch_size": batch_size,
    "token_length": token_length
}

for epoch in range(num_epochs):  # loop over the dataset multiple times

    with tqdm(train_dataloader, unit="batch", position=0, leave=True) as tepoch:
        train_loss = 0.0
        model.train()
        for i, (ids, mask, scores) in enumerate(tepoch):
            tepoch.set_description(f"Epoch {epoch}")
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(ids.to(device), mask.to(device))

            loss = criterion(outputs, scores.to(device))
            loss.backward()
            # optional gradient clipping
            # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            optimizer.step()
            train_loss += loss.item()
            # print statistics
            tepoch.set_postfix(loss=loss.item())
            wandb.log({"train_loss": loss.item()})
            wandb.watch(model)

    valid_loss = 0.0
    model.eval()
    all_validation_predictions = []
    for i, (ids, mask, scores) in enumerate(eval_dataloader):
        outputs = model(ids, mask)
        all_validation_predictions.append(outputs.cpu().detach().numpy())
        loss = criterion(outputs, scores)
        valid_loss += loss.item()

    all_validation_predictions = np.concatenate(all_validation_predictions)
    r2 = r2_score(val_scores, all_validation_predictions)
    valid_loss = valid_loss / len(eval_dataloader)
    print(
        f'\n Epoch {i + 1} \t\t Training Loss: {train_loss / len(train_dataloader)} \t\t Validation Loss: {valid_loss}')
    # if validation loss improves, save the new model parameters
    if min_valid_loss > valid_loss:
        print(f'\n Validation Loss Decreased({min_valid_loss:.6f}--->{valid_loss:.6f}) \t Saving The Model')
        min_valid_loss = valid_loss
        # Saving State Dict
        torch.save(model.state_dict(), out_name + '_best_val.pth')
    wandb.log({"val_loss": valid_loss, "val_r2": r2})

print('Finished Training')
print('Calculate Test Evaluation')
test_loss = 0.0
model.eval()  # Optional when not using Model Specific layer
all_test_predictions = []

for i, (ids, mask, scores) in enumerate(test_dataloader):
    outputs = model(ids, mask)
    all_test_predictions.append(outputs.cpu().detach().numpy())

    loss = criterion(outputs, scores)
    test_loss += loss.item()

all_test_predictions = np.concatenate(all_test_predictions)
r2 = r2_score(test_scores, all_test_predictions)
test_loss = test_loss / len(test_dataloader)
print(
    f'TestLoss: {test_loss}, TestR2: {r2}')
wandb.log({"test_loss": test_loss, "test_r2": r2})

PATH = './' + out_name + '.pth'
torch.save(model.state_dict(), PATH)
