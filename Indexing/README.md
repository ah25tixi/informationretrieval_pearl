Indexing
================================================

The index-files used by the two retrievers are created by the classes in [index.py](../index.py) and saved in the folders index for the arguments and sentences_index for the sentences.

The folder arg_corpus contains the arguments and setnences as CSV-files.