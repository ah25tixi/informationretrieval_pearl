import pandas as pd
from ast import literal_eval
from multiprocessing import Pool
from tqdm.auto import tqdm
import re
import unicodedata

def unicode_character_name(char):
    try:
        return unicodedata.name(char)
    except ValueError:
        return None

# Generate all Unicode characters with their names
all_unicode_characters = []
for n in range(0, 0x10ffff):    # Unicode planes 0-16
    char = chr(n)               # Python 3
    #char = unichr(n)           # Python 2
    name = unicode_character_name(char)
    if name:
        all_unicode_characters.append((char, name))
# List of all quotation mark characters to be replaced by '
quotation_marks = [char for char, name in all_unicode_characters if 'QUOTATION MARK' in name]

with open("../Indexing/args_corpus/sentences_processed_args.csv", 'w') as outf:
    new_line = 'arg_ids,sen_id,sentence,conclusion,stance,quality\n'
    outf.write(new_line)
    for i in range(1,38):
        print('chunk {}'.format(i))
        df = pd.read_csv('../Indexing/args_corpus/args_processed_part_{}.csv'.format(i),
                         usecols=['id', 'conclusion', 'premises', 'sentences'])# , nrows=10)
        quality_df = pd.read_csv('../Quality/out/No Premise/args_processed_part_{}_quality_no_premise.csv'.format(i))
        df['sentences'] = df['sentences'].apply(literal_eval)
        df['premises'] = df['premises'].apply(literal_eval)

        for index, row in tqdm(df.iterrows()):
            args_id = row['id']
            conclusion = row['conclusion']
            conclusion = conclusion.replace("\n", " ")
            for quote in quotation_marks:
                conclusion = conclusion.replace(quote, "'")
            stance = row['premises'][0]['stance']
            quality = quality_df.iloc[index]['quality']
            for sent in row['sentences']:
                sen_id = sent['sent_id']
                sentence = sent['sent_text']
                if len(sentence) <= 60:
                    continue
                sentence = sentence.replace("\n"," ")
                for quote in quotation_marks:
                    sentence = sentence.replace(quote, "'")
                new_line = '{args_id},{sen_id},"{sentence}","{conclusion}",{stance},{quality}\n'.format(
                    args_id=args_id, sen_id=sen_id, sentence=sentence, conclusion=conclusion, stance=stance, quality=quality
                )
                outf.write(new_line)


