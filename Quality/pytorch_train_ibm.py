import pandas as pd
import numpy as np
from transformers import AutoTokenizer
import torch
from torch import nn
from torch.utils.data import DataLoader
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from tqdm.auto import tqdm
from classes import CustomBERTModel, QualityDataset
import wandb
from scipy.stats import pearsonr, spearmanr

wandb.init(project="ArgumentQualityPrediction_sentence", entity="jonasp")
device = 'cuda'
out_name = "sentence_quality_prediction_unfrozen"

dataset = pd.read_csv('data/arg_quality_rank_30k.csv', usecols=['argument', 'set', 'WA']) #, nrows=100)

train_texts = dataset.loc[dataset['set'] == 'train']['argument'].to_list()
val_texts = dataset.loc[dataset['set'] == 'dev']['argument'].to_list()
test_texts = dataset.loc[dataset['set'] == 'test']['argument'].to_list()

train_scores = dataset.loc[dataset['set'] == 'train']['WA'].to_numpy().reshape(-1, 1)
val_scores = dataset.loc[dataset['set'] == 'dev']['WA'].to_numpy().reshape(-1, 1)
test_scores = dataset.loc[dataset['set'] == 'test']['WA'].to_numpy().reshape(-1, 1)

# Tokenize text input
token_length = 512
tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", model_max_length=token_length)

train_encodings = tokenizer(train_texts, truncation=True, padding=True, return_tensors="pt")
val_encodings = tokenizer(val_texts, truncation=True, padding=True, return_tensors="pt")
test_encodings = tokenizer(test_texts, truncation=True, padding=True, return_tensors="pt")

train_dataset = QualityDataset(train_encodings, train_scores)
val_dataset = QualityDataset(val_encodings, val_scores)
test_dataset = QualityDataset(test_encodings, test_scores)

model = CustomBERTModel().to(device)

# Freeze pretrained bert layers
for param in model.bert.parameters():
    param.requires_grad = False

# Parameters from [Exploring BERT Synonyms and Quality Prediction
# for Argument Retrieval by Green, Moroldo, Valente]
batch_size = 32
learning_rate = 2e-5
weight_decay = 0.0005
num_epochs = 5

train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)
eval_dataloader = DataLoader(val_dataset, batch_size=batch_size)
test_dataloader = DataLoader(test_dataset, batch_size=batch_size)

criterion = nn.MSELoss()

optimizer = optim.AdamW(model.parameters(), lr=learning_rate, weight_decay=weight_decay)

num_training_steps = num_epochs * len(train_dataloader)
min_valid_loss = np.inf

wandb.config = {
    "learning_rate": learning_rate,
    "weight_decay": weight_decay,
    "epochs": num_epochs,
    "batch_size": batch_size,
    "token_length": token_length
}

for epoch in range(num_epochs):  # loop over the dataset multiple times

    with tqdm(train_dataloader, unit="batch", position=0, leave=True) as tepoch:
        train_loss = 0.0
        model.train()
        for i, (ids, mask, scores) in enumerate(tepoch):
            tepoch.set_description(f"Epoch {epoch}")
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(ids.to(device), mask.to(device))

            loss = criterion(outputs, scores.to(device))
            loss.backward()
            # optional gradient clipping
            # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            optimizer.step()
            train_loss += loss.item()
            # print statistics
            tepoch.set_postfix(loss=loss.item())
            wandb.log({"train_loss": loss.item()})
            wandb.watch(model)

    valid_loss = 0.0
    model.eval()
    all_validation_predictions = []
    for i, (ids, mask, scores) in enumerate(eval_dataloader):
        outputs = model(ids.to(device), mask.to(device))
        all_validation_predictions.append(outputs.cpu().detach().numpy())
        loss = criterion(outputs, scores.to(device))
        valid_loss += loss.item()

    all_validation_predictions = np.concatenate(all_validation_predictions)
    r2 = r2_score(val_scores, all_validation_predictions)
    valid_loss = valid_loss / len(eval_dataloader)
    pearson = pearsonr(val_scores.flatten(), all_validation_predictions.flatten())
    spearman = spearmanr(val_scores.flatten(), all_validation_predictions.flatten())
    out_dict = {"valid_loss": valid_loss, "valid_r2": r2, "valid_pearson": pearson[0], "valid_spearman": spearman.correlation}
    print(out_dict)

    # if validation loss improves, save the new model parameters
    if min_valid_loss > valid_loss:
        print(f'\n Validation Loss Decreased({min_valid_loss:.6f}--->{valid_loss:.6f}) \t Saving The Model')
        min_valid_loss = valid_loss
        # Saving State Dict
        torch.save(model.state_dict(), out_name + '_best_val.pth')
    wandb.log(out_dict)

print('Finished Training')
print('Calculate Test Evaluation')
test_loss = 0.0
model.eval()  # Optional when not using Model Specific layer
all_test_predictions = []

for i, (ids, mask, scores) in enumerate(test_dataloader):
    outputs = model(ids.to(device), mask.to(device))
    all_test_predictions.append(outputs.cpu().detach().numpy())

    loss = criterion(outputs, scores.to(device))
    test_loss += loss.item()

all_test_predictions = np.concatenate(all_test_predictions)
r2 = r2_score(test_scores, all_test_predictions)
pearson = pearsonr(test_scores.flatten(), all_test_predictions.flatten())
spearman = spearmanr(test_scores.flatten(), all_test_predictions.flatten())
test_loss = test_loss / len(test_dataloader)
out_dict = {"test_loss": test_loss, "test_r2": r2, "test_pearson": pearson[0], "test_spearman": spearman.correlation}
print(out_dict)
wandb.log(out_dict)

PATH = './' + out_name + '.pth'
torch.save(model.state_dict(), PATH)
