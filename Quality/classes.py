from transformers import AutoModel
import torch
from torch import nn
from torch.utils.data import Dataset
import torch.nn.functional as F


# Define Bert model as described by [Exploring BERT Synonyms and Quality Prediction
# for Argument Retrieval by Green, Moroldo, Valente]
class CustomBERTModel(nn.Module):
    def __init__(self):
        super(CustomBERTModel, self).__init__()
        self.bert = AutoModel.from_pretrained("bert-base-uncased")
        self.fc1 = nn.Linear(3072, 100)
        self.fc2 = nn.Linear(100, 100)
        self.fc3 = nn.Linear(100, 1)
        self.activation = nn.Sigmoid()

    def forward(self, ids, mask):
        outputs = self.bert(
            ids,
            attention_mask=mask, output_hidden_states=True)

        hidden_states = outputs.hidden_states
        # concatenate hidden states of last four cls tokens
        cls_token_concat = torch.concat(
            [hidden_states[-4][:, 0, :],
             hidden_states[-3][:, 0, :],
             hidden_states[-2][:, 0, :],
             hidden_states[-1][:, 0, :]], dim=1)
        x = self.fc1(cls_token_concat)
        x = F.relu(x)
        # x = F.alpha_dropout(x, 0.5) # change relu to selu if alpha dropout
        x = self.fc2(x)
        x = F.relu(x)
        # x = F.alpha_dropout(x, 0.5)
        x = self.fc3(x)
        return x


# Bring training, validation and test data in proper form.
# Extracts Token Ids and attention mask from tokenized text
class QualityDataset(Dataset):
    def __init__(self, data, targets):
        self.data = data
        self.targets = torch.FloatTensor(targets)

    def __getitem__(self, index):
        x = self.data[index]
        y = self.targets[index]
        return torch.LongTensor(x.ids), torch.LongTensor(x.attention_mask), y

    def __len__(self):
        return len(self.data.encodings)


# Bring data in proper from for quality prediction.
# Extracts Token Ids and attention mask from tokenized text
class PredictionDataset(Dataset):
    def __init__(self, data):
        self.data = data

    def __getitem__(self, index):
        x = self.data[index]
        return torch.LongTensor(x.ids), torch.LongTensor(x.attention_mask)

    def __len__(self):
        return len(self.data.encodings)
