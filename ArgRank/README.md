ArgRank for global relevance
================================================
## Related work
The [ArgRank](https://aclanthology.org/E17-1105.pdf) is an adaptation of the PageRank-algorithm and was proposed by Wachsmuth et al. in 2017. The idea is to rank arguments based on their global relevance as estimated by reuses of their conclusion as a premise for other arguments. We implemented the ArgRank to rerank the arguments retrieved by the Dirichlet model.
<br>

## ArgGraphBuilder
The ArgGraphBuilder-class in [arg_rank.py](arg_rank.py) is used to find the edges of the argument graph. It processes all unique conclusions (i.e. ones that are different from the discussion title of their argument) and looks for premises in other arguments with a high semantic similarity. The semantic similarity is estimated from the cosine similarity between encodings calculated using the [SentenceMatcher](../Utilities/sentence_similarity.py)-class. By default, a reuse of a premise is assumed for cosine similarity greater than 0.7. This value can, however, be set as a parameter of the ArgGraphBuilder.

The output is a CSV-file containing all edges with the following information:
<br><br/>

|Name           | Description                                                              |
|:--------------|:-------------------------------------------------------------------------|
|target_id      |The id of the argument of which the conclusion was reused                 |
|origin_id      |The id of the argument that uses the conclusion as a premise              |
|num_premises   |The number of premises of the reusing argument                            |
|cos_sim        |The similarity between the premised identified as reuse and the conclusion|

<br>
The argument graph can be visualized as follows with an arrow indicating that an argument uses the conclusion of the argument it is pointing at. The values next to the arrows indicate the cosine similarity.
 
<br><br/>
![Argument Graph](../Img/ArgGraph.png)
<br><br/>

## ArgRanker
The CSV-file create by the ArgGraphBuilder can be used by the ArgRanker to calculate the ArgRank of all arguments in a specific file. The parameters of this class can be used to specify the minimum cosine similarity of an edge to be considered as well as alpha, the weighting factor between the ground relevance and recursive relevance. Lastly, the cosine similarity of an edge can also be used to weigh the recursive relevance, thereby rewarding a high and punishing a low similarity score.

When experimenting with different combinations, we found not weighting by cosine similarity in combination with min_cosine_sim=0.75 (0.8) and alpha=0.3 (0.4) to achieve the best results.

The resulting ArgRanks are saved as a new column in the DataFrame containing all arguments, which is in turn saved as a CSV-file.
