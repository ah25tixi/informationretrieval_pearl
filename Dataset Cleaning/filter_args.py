import numpy as np
import pandas as pd

all_args = pd.read_csv('../Indexing/args_corpus/args.csv')
#sents = pd.read_csv('../Indexing/args_corpus/sentences.csv')

all_args = all_args.drop(columns=['Unnamed: 0'])

isarg = pd.read_csv('../Indexing/args_corpus/isarg.csv')

args = all_args.loc[isarg['isArgument?'] == True]
not_arg = all_args.loc[isarg['isArgument?'] == False]
args.to_csv('../Indexing/args_corpus/args.csv', index=False)
not_arg.to_csv('../Indexing/args_corpus/non_args.csv', index=False)