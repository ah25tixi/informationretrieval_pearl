import pandas as pd

v1 = pd.read_csv('Version 1/analysis_output/graph.csv', index_col=0)
v1 = v1.rename(columns={'d_evaluation': 'd_evaluation_1',
                        'Argumentativeness': 'Argumentativeness_1',
                        'Sentence_Coherence': 'Sentence_Coherence_1',
                        'Represents_Argument': 'Represents_Argument_1',
                        'Precision': 'Precision_1',
                        'DCG': 'DCG_1'})
v2_NMR = pd.read_csv('Version 2/analysis_output/NMR_graph.csv', index_col=0)
v2_NMR = v2_NMR.rename(columns={'d_evaluation': 'd_evaluation_2_NMR',
                                'Argumentativeness': 'Argumentativeness_2_NMR',
                                'Sentence_Coherence': 'Sentence_Coherence_2_NMR',
                                'Represents_Argument': 'Represents_Argument_2_NMR',
                                'Precision': 'Precision_2_NMR',
                                'DCG': 'DCG_2_NMR'})
v2_NSP = pd.read_csv('Version 2/analysis_output/NSP_graph.csv', index_col=0)
v2_NSP = v2_NSP.rename(columns={'d_evaluation': 'd_evaluation_2_NSP',
                                'Argumentativeness': 'Argumentativeness_2_NSP',
                                'Sentence_Coherence': 'Sentence_Coherence_2_NSP',
                                'Represents_Argument': 'Represents_Argument_2_NSP',
                                'Precision': 'Precision_2_NSP',
                                'DCG': 'DCG_2_NSP'})
v2_Partner = pd.read_csv('Version 2/analysis_output/Partner_graph.csv', index_col=0)
v2_Partner = v2_Partner.rename(columns={'d_evaluation': 'd_evaluation_2_Partner',
                                        'Argumentativeness': 'Argumentativeness_2_Partner',
                                        'Sentence_Coherence': 'Sentence_Coherence_2_Partner',
                                        'Represents_Argument': 'Represents_Argument_2_Partner',
                                        'Precision': 'Precision_2_Partner',
                                        'DCG': 'DCG_2_Partner'})
concat = pd.concat([v1, v2_NMR, v2_NSP, v2_Partner], axis=1)
concat = concat.drop([1, 2, 4, 5, 7, 8, 10, 11, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 38, 40, 41, 42, 43, 44, 46, 50])
concat.to_csv('compare_output/concat.csv')

concat.plot(y=["d_evaluation_1", "d_evaluation_2_NMR", "d_evaluation_2_NSP", "d_evaluation_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/d_evaluation_compare_version.png')
concat.plot(y=["Argumentativeness_1", "Argumentativeness_2_NMR", "Argumentativeness_2_NSP", "Argumentativeness_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/Argumentativeness_compare_version.png')
concat.plot(y=["Sentence_Coherence_1", "Sentence_Coherence_2_NMR", "Sentence_Coherence_2_NSP", "Sentence_Coherence_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/Sentence_Coherence_compare_version.png')
concat.plot(y=["Represents_Argument_1", "Represents_Argument_2_NMR", "Represents_Argument_2_NSP", "Represents_Argument_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/Represents_Argument_compare_version.png')
concat.plot(y=["Precision_1", "Precision_2_NMR", "Precision_2_NSP", "Precision_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/Precision_compare_version.png')
concat.plot(y=["DCG_1", "DCG_2_NMR", "DCG_2_NSP", "DCG_2_Partner"],
            kind="bar", figsize=(9, 8)).figure.savefig('compare_output/DCG_compare_version.png')
