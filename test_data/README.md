# Touché Task 1

Resources for the first project available as part of the "[Advanced Information Retrieval](https://temir.org/teaching/information-retrieval-ws-2020-21/information-retrieval-ws-2020-21.html)" course at Leipzig University.

# Source Dataset
## [args.me Corpus](https://zenodo.org/record/3734893#.X5BreS337OQ)

[Ajjour et al. Data Acquisition for Argument Search: The args.me corpus. (KI 2019)](https://webis.de/publications.html#stein_2019q)

The main data source for the project - this is the data your search engine is suppoosed to index. 

You can download the complete data from Zenodo: [args.me corpus](https://zenodo.org/record/3734893#.X5BreS337OQ), where you can also find further information. 
Keep in mind that there are multiple .zip files, each containing a part of the complete data.


# Training & Evaluation Datasets

## [Touche Dataset](./Dataset\ Touche\ 2020)

[Bondarenko et al. Overview of Touché 2020: Argument Retrieval. (CLEF 2020 Evaluation Labs)](https://webis.de/publications.html#stein_2020v)

This directory contains data from the first shared subtask on Argument Retrieval, [Touché 2020](https://events.webis.de/touche-20/).
It consists of three files:

The `topics.xml` file contains exemplary search topics as denoted below:

```xml
<topics>
    <topic>
        <number>1</number>
        <title>Should Teachers Get Tenure?</title>
    </topic>

    <topic>
        <number>2</number>
        <title>Is Vaping with E-Cigarettes Safe?</title>
    </topic>
    
    <topic>
        <number>3</number>
        <title>Should Insider Trading Be Allowed?</title>
    </topic>

    <topic>
        <number>4</number>
        <title>Should Corporal Punishment Be Used in Schools?</title>
    </topic>
    
    ...
    ...
    ...
    
</topics>
```

`<number>` denotes a unique topic identifer and `<title>` denotes the query as to be entered into the retrieval system.

The other two files in this folder ([`relevance-args-v1.qrels`](https://git.webis.de/code-teaching/readings/information-retrieval-ss20-leipzig/-/blob/master/Dataset%20Touche%202020/relevance-args-v1.qrels) and [`relevance-args-v2.qrels`](https://git.webis.de/code-teaching/readings/information-retrieval-ss20-leipzig/-/blob/master/Dataset%20Touche%202020/relevance-args-v2.qrels)) contain relevance judgements for each of the two versions of the args.me corpus, respectively. `v2` will likely be the more relevant one for you.

Qrel files are formatted according the the standard TREC layout with 4 whitespace-separated columns: `qid`, `Q0`, `docid`, `relevancy`, where `qid` refers to the topic number (as also found in `topics.xml`), `Q0` is deprecated and contains only zeroes, `docid` is an ID identfying a document in the `args.me` corpus, and `relevancy` denotes the relevance of a a document to a query on a scale from -2 to 4 (irrelevant to highly relevant).

## [ACL20 Dataset](./Dataset\ ACL20)

[Gienapp et al. Efficient Pairwise Annotation of Argument Quality. (ACL 2020)](https://webis.de/publications.html?q=gienapp#stein_2020o). 

The ACL20 data consists of 3 separate data sets. Their structure is given below. Each key represents a column name, with details about the contained data in the explanation field. Primary keys are marked in **bold**. If a combined key is used, all entries that the combined key is composed of are marked. Foreign keys that can be used to reference other tables are marked in *italics*.

###### Argument Dataset
| Key                 | Explanation                                                                                       |
|---------------------|---------------------------------------------------------------------------------------------------|
| ***Topic ID***      | Unique identifier for the topic context the item was judged in                                    |
| **Argument ID**     | Unique identifier for the item in regards to the discussion it is part of                         |
| **Discussion ID**   | Unique identifier of the discussion the item is part of                                           |
| Is Argument?        | Boolean value, indicating wether the item is an argument, or not                                  |
| Stance              | Denotes the stance of the item, can be Pro, Con or Not specified                                  |
| Relevance           | Relevance score, z-normalised  																	  |
| Logical Quality     | Logical quality score, z-normalised     														  |
| Rhetorical Quality  | Rhetorical quality score, z-normalised     														  |
| Dialectical Quality | Dialectical quality score, z-normalised     													  |
| Combined Quality 	  | Combined quality score, z-normalised     													 	  |
| Premise             | Text of the items' premise                                                                        |
| Text Length         | Word Count of the premise                                                                         |


###### Ranking Dataset
| Key               | Explanation                                                                   |
|-------------------|-------------------------------------------------------------------------------|
| ***Topic ID***    | Unique identifier for the topic context                                       |
| **Model**         | Name of the model the ranking this entry stems from was obtained with         |
| **Rank**          | The rank of the argument in the respective engines ranking                    |
| *Argument ID*     | Unique identifier for the argument in regards to the discussion it is part of |
| *Discussion ID*   | Unique identifier of the discussion the argument is part of                   |



###### Topic Dataset
| Key                       | Explanation                                                       |
|---------------------------|-------------------------------------------------------------------|
| ***Topic ID***            | Unique identifier for the topic                                   |
| Category                  | Thematical category the topic belongs to                          |
| Long Query                | Long query, used as input for the retrieval models                |
| Short Query               | Shortened form of the query                						|

## [SIGIR19 Dataset](./Dataset\ SIGIR19)
[Potthast et al. *Argument Search: Assessing Argument Relevance.* (SIGIR 2019)](https://webis.de/publications.html#stein_2019k)

The SIGIR19 data consists of 4 separate data sets. Their structure is given below. Each key represents a column name, with details about the contained data in the explanation field. Primary keys are marked in **bold**. If a combined key is used, all entries that the combined key is composed of are marked. Foreign keys that can be used to reference other tables are marked in *italics*.

**The SIGIR19 data is considered deprecated in favor of ACL20 (for quality) and Touche (for relevance). Please refer to the other datasets for the current & most accurate data.**

###### Argument Dataset
| Key                 | Explanation                                                                                       |
|---------------------|---------------------------------------------------------------------------------------------------|
| ***Topic ID***      | Unique identifier for the topic context the item was judged in                                    |
| **Argument ID**     | Unique identifier for the item in regards to the discussion it is part of                         |
| **Discussion ID**   | Unique identifier of the discussion the item is part of                                           |
| Is Argument?        | Boolean value, indicating wether the item is an argument, or not                                  |
| Stance              | Denotes the stance of the item, can be Pro, Con or Not specified                                  |
| Relevance           | Relevance score as judged by the annotator, on a scale of 1 (not relevant) to 4 (very relevant)   |
| Logical Quality     | Logical quality score as judged by the annotator, on a scale of 1 (very bad) to 4 (very good)     |
| Rhetorical Quality  | Rhetorical quality score as judged by the annotator, on a scale of 1 (very bad) to 4 (very good)  |
| Dialectical Quality | Dialectical quality score as judged by the annotator, on a scale of 1 (very bad) to 4 (very good) |
| Premise             | Text of the items' premise                                                                        |
| Conclusion          | Text of the items' conclusion                                                                     |
| Comment             | Optional Comment made by the annotator                                                            |

###### Ranking Dataset
| Key               | Explanation                                                                   |
|-------------------|-------------------------------------------------------------------------------|
| ***Topic ID***    | Unique identifier for the topic context                                       |
| **Engine**        | Name of the engine the ranking this entry stems from was obtained with        |
| **Rank**          | The rank of the argument in the respective engines ranking                    |
| *Argument ID*     | Unique identifier for the argument in regards to the discussion it is part of |
| *Discussion ID*   | Unique identifier of the discussion the argument is part of                   |

###### Annotator Dataset 
| Key               | Explanation                              |
|-------------------|------------------------------------------|
| ***Topic ID***    | ID of the topic judged by this annotator |
| Age               | Age of the annotator                     |
| Gender            | Gender of the annotator                  |
| Comment           | Annotators’ comments about the study     |

###### Topic Dataset
| Key                       | Explanation                                                                                                                                                                                                                                   |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ***Topic ID***            | Unique identifier for the topic                                                                                                                                                                                                               |
| Biased                    | Boolean value indicating whether the topic is biased or not, i.e. if the annotator was tasked to adopt a predetermined stance (topic thesis)                                                                                                  |
| Annotator Stance          | Stance of the annotator, can be *I agree*, *I disagree* or *Neutral*; if the topic is biased, the stance is determined in regards to the topic description; if the topic is unbiased, the stance is determined in regards to the topic thesis |
| Thesis                    | Predetermined stance for unbiased topics, empty otherwise                                                                                                                                                                                     |
| Description               | Text description of the topic                                                                                                                                                                                                                 |
| Query                     | Query used for this topic as input for the retrieval models                                                                                                                                                                                   |
