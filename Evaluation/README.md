Evaluation
================================================

The version folders contain the results retrieved by several versions of our retrieval model.
<br>

|Folder         | Results based on                                                            |
|:--------------|:----------------------------------------------------------------------------|
|Version 1      |The vertical prototype                                                       |
|Version 2      |The different matching approaches (MMR, NSP, Neighbour)                      |
|Version 3      |The final model with blocklist, ArgRank and weighted sum of relevance scores |

The excel-sheet [Evaluation_Sheet.xlsx](Evaluation_Sheet.xlsx) summarizes all evaluation scores and provides an overview on the performance of the different approaches.