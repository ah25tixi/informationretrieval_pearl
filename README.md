Pearl Retriever For Argumentative Sentence Pairs
================================================

The pearl retriever retrieves argumentative sentence pairs from the [args.me](https://git.webis.de/code-research/arguana/args/args-framework)-corpus using [PyTerrier](https://pyterrier.readthedocs.io/en/latest/), a Python API for Terrier.

## Retrieval pipeline

![pearl retrieval pipeline](Img/Retrieval_Pipeline.png)

 
### Argument Retrieval

The argument retriever uses [DirichletLM](http://sigir.hosting.acm.org/wp-content/uploads/2017/06/p268.pdf)-based retrieval to obtain relevant arguments for a given query. These arguments are then filtered using pre-caclulated quality scores obtained with a BERT-based classifier. If specified by the user, [ArgRank](https://aclanthology.org/E17-1105.pdf)-based reranking can be applied to the results.

Finally, the remaining arguments are used to restrict the results of the sentence retriever to only sentences from those arguments. 


### Sentence Retrieval

The sentence retriever uses [DPH](https://lintool.github.io/robust04-analysis-papers/Amati2006_Chapter_FrequentistAndBayesianApproach.pdf)-based retrieval to obtain individual sentences for a given query. The DPH-model is used to put strong emphasis on terms of the query that, while comparatively rare in the corpus, occur in the retrieved sentences. As stated above, sentences that are not from relevant arguments are discarded. 

The sentences are then ranked based on their own relevance score and the relevance score of their parent-argument and filtered using pre-calculated quality scores from a different, BERT-based quality classifier.

Finally, the sentences are matched with their optimal neighbour in the parent argument. The optimal neighbour is either the preceeding or succeeding sentence and precalculated using the same BERT-based quality classifier as for individual sentences.



## How to use the retriever

The full retrieval is summarized in the PearlRetriever-class of [pearl.py](pearl.py). Its behavior can be specified using a range of attributes. The main way to obtain retrieval results is through the retrieveToCSV-method that takes a Pandas-Series of queries as input and outputs the most relevant sentence-pairs to a CSV-file. The underlying retrieval methods are implemented in [retrieve.py](retrieve.py).


