import pandas as pd
import numpy as np
import sys
from sklearn.metrics.pairwise import cosine_similarity
from os.path import exists

from Utilities.sentence_similarity import SentenceMatcher
from Utilities.Sentiment_Analysis.sentiment_analysis import SentimentAnalyzer
from retrieve import ArgsRetriever

# Don't show warnings about chained assignments
pd.options.mode.chained_assignment = None

ARG_PATH = "../Indexing/args_corpus/args.csv"
SEN_PATH = "../Indexing/args_corpus/sentences.csv"
EDGELIST_PATH = "Edgelist_discussion_70.csv"
OUT_DIR = "../Data/ArgRanks/"

# Libraries to perform matrix multiplication using CUDA
import pycuda.gpuarray as gpuarray
import skcuda.linalg as linalg

# Init
import pycuda.autoinit
linalg.init()


class ArgGraphBuilder:
    def __init__(self,
                 search_space="discussion",
                 arg_path=ARG_PATH,
                 sen_path=SEN_PATH,
                 min_similarity=0.7,
                 num_retrieval_results=30):
        """
        :param search_space:            String defining the space in which related arguments are search
                                        ['discussion' or 'retrieval']
        :param arg_path:                Path to the argument csv
        :param sen_path:                Path to the sentences csv
        :param min_similarity:          Minimum cosine similarity required for an edge to be saved
        :param num_retrieval_results:   How many results are to be retrieved per argument (search_space = 'retrieval')
        """

        self.arg_path = arg_path
        self.sen_path = sen_path
        self.min_similarity = min_similarity
        self.search_space = search_space
        self.num_retrieval_results = num_retrieval_results

        # Create the filename based on the search space and minimum similarity
        self.filename = "Edgelist_" + search_space + "_" + str(int(min_similarity*100)) + ".csv"

        # Preparation based on search_space
        print("=" * 50)
        if search_space == "discussion":
            # Read in the dataframes
            print("Preparing the dataframes...")
            self.readArgsCSV()

        elif search_space == "retrieval":
            print("Preparing the argument retriever and loading sentences...")
            self.ArgRetriever = ArgsRetriever(arg_path=arg_path,
                                              num_results=num_retrieval_results,
                                              index_path="../Indexing/index/data.properties")
            self.argDF = self.ArgRetriever.argDF
            self.argDF.rename(columns={'docno': 'arg_id'}, inplace=True)

            # Set the sentiment analyzer for stance evaluation
            self.SentimentAnalyzer = SentimentAnalyzer()

        else:
            sys.exit("Please provide a valid search space: 'discussion' or 'retrieval'.")

        self.readSenCSV()
        print("=" * 50)

        # Set the Sentence Matcher
        print("\n" + "=" * 50)
        print("Preparing the Sentence Matcher...")
        self.SentenceMatcher = SentenceMatcher()
        print("=" * 50)


    # Function that calculates argument graphs based on cosine similarity
    # - The graphs are saved in a csv-file in an edge list format
    # - Row A,B,X,Y: Argument B uses conclusion of A as premise; B has X premises and the match has cos_sim Y
    def createArgGraphs(self):
        num_arguments = len(self.argDF)
        new_content = True
        # Create a file with header (if non-existent)
        if exists(self.filename):
            new_content = False
        else:
            with open(self.filename, "w") as outf:
                # Write header
                outf.write("target_id,origin_id,num_premises,cos_sim\n")
                outf.close()

        # If a file was found, get the last processed argument from the file with the lowest threshold
        last_id = ""
        if not new_content:
            last_id = self.getLastID(self.filename)

        # Loop over all arguments
        for i, row in self.argDF.iterrows():
            print("Processing argument {}/{}".format(i, num_arguments))

            conclusion = row["conclusion"]
            discussion_title = row["discussion_title"]

            # Skip the argument if the discussion title is the same as the conclusion
            if conclusion == discussion_title:
                continue

            # Otherwise get additional information (premises are only required for ArgRank through retrieval)
            arg_id = row["arg_id"]
            stance = row["stance"]
            premises = row["premises"]

            # Check if the arg_id is the last that was processed to set new_content to true
            # If not, pass this argument
            if not new_content:
                if arg_id == last_id:
                    new_content = True
                    print("-" * 50)
                    print("Starting to write new edges of the argument graph")
                    print("-" * 50)
                continue

            # Get premises of arguments that have a cosine similarity to the conclusion above min_similarity
            matched_sentences = self.matchSentences(conclusion=conclusion,
                                                    discussion_title=discussion_title,
                                                    arg_id=arg_id,
                                                    stance=stance,
                                                    premises=premises)

            # Write the results into the file
            self.writeEdges(arg_id=arg_id, sentences=matched_sentences)



    # Function to write the edges to the csv-file
    def writeEdges(self, arg_id, sentences):
        """
        :param arg_id:      ID of the argument that is referenced by other arguments
        :param sentences:   Dataframe of sentences that have sufficently high cos_sim with the arguments conclusion
        """

        with open(self.filename, "a") as outf:
            # Reduce to matched arguments (id, num_premises, and cos_sim columns) and write results
            matched_args = sentences.drop_duplicates(subset=["arg_id"])[["arg_id", "num_premises", "cos_sim"]]

            # Write edges of the Arg_Graph to the file
            for j, arg_row in matched_args.iterrows():
                reference_id = arg_row["arg_id"]
                num_premises = arg_row["num_premises"]
                cos_sim = arg_row["cos_sim"]

                outf.write(arg_id + "," + reference_id + "," + str(num_premises) + "," + str(cos_sim) + "\n")

            outf.close()

    # Function that takes in argument parameters
    # Returns a df of sentences with cosine values above min_similarity
    def matchSentences(self, conclusion, discussion_title, arg_id, stance, premises):
        """
        :param conclusion:          The arguments conclusion used for matching
        :param discussion_title:    Title of the discussion of the argument
        :param arg_id:              ID of the argument for which references are searched
        :param stance:              Stance of the argument
        :param premises:            Premises of the argument (Used in stance evaluation)
        :return:                    Selection of sentences with sufficiently high cos_sim to the conclusion
        """

        # Filter the argument ids based on the search space
        # - Version 1: From the same discussion
        # - Version 2: By retrieving on the discussion title
        if self.search_space == "discussion":
            # Filter the argument ids: Same discussion, same stance and different arg_id
            filtered_arg_ids = self.argDF.loc[((self.argDF['discussion_title'] == discussion_title) &
                                               (self.argDF['stance'] == stance) &
                                               (self.argDF['arg_id'] != arg_id))]["arg_id"]

        else:
            # Get arg_ids based on retrieval on the discussion title
            filtered_arg_ids = self.getArgIDsThroughRetrieval(discussion_title=discussion_title,
                                                              premises=premises,
                                                              arg_id=arg_id,
                                                              stance=stance)

        # Use the filtered ids to get the selection of sentences and keep only premises [sen_id contains "PREMISE"]
        filtered_sen = self.senDF.loc[self.senDF["arg_id"].isin(filtered_arg_ids)]
        filtered_sen = filtered_sen[filtered_sen.sen_id.str.contains('|'.join(['PREMISE']))]

        # Count the number of premises per argument and store them in a separate column
        filtered_sen = self.countPremises(filtered_sen)

        # Get cosine similarity between the conclusion and premises of other arguments
        cosine = self.calculateCosine(conclusion, filtered_sen["sentence"].to_list())

        # Find matches (If cosine > min_similarity) and store the cosine values
        matches = cosine > self.min_similarity
        filtered_sen["match"] = matches.tolist()
        filtered_sen["cos_sim"] = cosine

        # Return sentences filtered on matches (Boolean column)
        return filtered_sen.loc[filtered_sen["match"]]


    # Function to count the number of premises of an argument
    def countPremises(self, filtered_sen):
        """
        :param filtered_sen:    Dataframe of sentences with arg_ids used for premise counting
        :return:                Updated dataframe with a column for number of premises
        """
        # Create an empty column
        filtered_sen.loc[:, "num_premises"] = pd.Series(dtype="float64")
        argID = ""
        count = 1

        # Loop over the rows and count the number of occurences of arg_id
        for i, row in filtered_sen.iterrows():
            # If the arg_id was not yet counted, update the ard_id and counts
            if (row["arg_id"] != argID):
                argID = row["arg_id"]
                count = filtered_sen["arg_id"].value_counts()[argID]

            filtered_sen.loc[i, "num_premises"] = count

        return filtered_sen


    # Function to caclulate the embeddings and look for similarities between a given conclusion and the premises
    def calculateCosine(self, conclusion, premises_list):
        premises_list.insert(0, conclusion)

        # Encode the str_list and return the cosine similarities
        # (only the 1st column from the 2nd row downwards is required )
        str_encodings = self.SentenceMatcher.model.encode(premises_list)
        return cosine_similarity(np.array(str_encodings))[1:, 0]


    # Function to get the id of the last argument that was processed
    def getLastID(self, filepath):
        df = pd.read_csv(filepath)
        return df["arg_id"].iloc[-1]


    def retrieveArgs(self, query):
        if not hasattr(self, "ArgRetriever"):
            self.ArgRetriever = ArgsRetriever(arg_path=self.arg_path,
                                              num_results=self.num_retrieval_results,
                                              index_path="../Indexing/index/data.properties")

        self.ArgRetriever.retrieve(query)

        # Save the retrieved arguments
        self.retrievedArgs = self.ArgRetriever.retrievalDF


    # Function that retrieves arguments based on the discussion title
    def getArgIDsThroughRetrieval(self, discussion_title, premises, arg_id, stance):
        """
        :param discussion_title:    Discussion title of the argument (Used for retrieval)
        :param premises:            Premises of the argument (Used in stance evaluation)
        :param arg_id:              ID of the argument for which references are searched
        :param stance:              Stance of the argument
        :return:                    ID's of other arguments that were retrieved based on the discussion title
        """

        # Retrieve the arguments and discard the one with the same arg_id
        self.retrieveArgs(discussion_title)
        filtered_args = self.retrievedArgs.loc[self.retrievedArgs['arg_id'] != arg_id]

        # Try to update the stance using the sentiment analyzer
        stance = self.SentimentAnalyzer.getStanceString(string=premises, stance=stance)
        filtered_args = self.SentimentAnalyzer.setStanceArgDataframe(df=filtered_args)

        # Filter based on the stance
        filtered_args = filtered_args.loc[filtered_args['stance'] == stance]

        # Return the ids
        return filtered_args["docno"]


    # =======================================================
    # Functions to read in the arguments and sentences
    # =======================================================

    def readArgsCSV(self, chunksize=10000):
        """
        Function that reads in the argument body to be used to create the arg graph
        :param chunksize: The number of lines to be read in from the CSV at once
        """
        self.argDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.arg_path, chunksize=chunksize)
        for i, chunk in enumerate(rows):
            # Preprocess the chunk
            chunk.rename(columns={'id': 'arg_id'}, inplace=True)
            chunk.drop('quality', axis=1, inplace=True)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.argDF = chunk
            else:
                self.argDF = pd.concat([self.argDF, chunk], axis=0)

            print("Read {} arguments".format(len(self.argDF)))

    def readSenCSV(self, chunksize=10000):
        """
        Function that reads in the sentences body to be used in retrieval
        :param chunksize: The number of lines to be read in from the CSV at once
        """
        self.senDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.sen_path, chunksize=chunksize)
        for i, chunk in enumerate(rows):
            # Preprocess the chunk
            chunk.rename(columns={'arg_ids': 'arg_id'}, inplace=True)
            dropcolums = ['quality', 'sent_quality', 'stance', 'conclusion']
            for column in dropcolums:
                chunk.drop(column, axis=1, inplace=True)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.senDF = chunk
            else:
                self.senDF = pd.concat([self.senDF, chunk], axis=0)

            print("Read {} sentences".format(len(self.senDF)))





class ArgRanker:
    def __init__(self,
                 edgelist_path=EDGELIST_PATH,
                 arg_path=ARG_PATH,
                 alpha=0.15,
                 min_cosine_sim=0.8,
                 cosine_weight=False,
                 groundRelBool=True,
                 convergDiff = 10**(-8)):

        """
        :param edgelist_path:       Path of the csv-file containing the edges of the ArgGraph
        :param arg_path:            Path of the csv-file containing the arguments
        :param alpha:               Damping factor for the calculation of the ArgRanks
        :param min_cosine_sim:      Minimum cos similarity between conclusion and premise for an edge to be considered
        :param cosine_weight:       True: Use the cosine similarity as a factor in the ArgRank (Otherwise factor=1)
        :param groundRelBool:       False: The ground relevance of each argument is set to 1;
                                    True: The ground relevance is calculated based on (1-alpha)/#arguments
                                    (Leads to stronger focus on arguments that are referred to by others)
        :param convergDiff:         The maximum difference between ArgRank iterations to assume convergence
        """

        print("=" * 50)
        print("Preparing the argument ranker...")
        self.alpha = alpha
        self.convergDiff = convergDiff

        # Read in the edgelist and discard all rows below cosine threshold
        self.edgelist = pd.read_csv(edgelist_path)
        self.edgelist = self.edgelist.loc[self.edgelist["cos_sim"] > min_cosine_sim]
        print("- Edgelist was read in")

        # Set the cos_sim-column to 1 if cosine_weights are not used in multiplication
        if not cosine_weight:
            self.edgelist["cos_sim"].values[:] = 1

        # Read in the arguments (To store the calculated argRanks)
        self.arg_path = arg_path
        self.readArgsCSV()
        print("- Arguments were read in")

        # Set the ground relevance
        self.groundRelBool = groundRelBool
        if not groundRelBool:
            self.groundRelevance = 1
        else:
            self.groundRelevance = (1-alpha)/len(self.argDF)

        # Create the outpath
        self.defineOutpath(edgelist_path,
                           min_cosine_sim,
                           cosine_weight)

        # Create the multiplication matrix
        self.createMultMatrix()
        print("- Multiplication matrix was created")
        print("=" * 50 + "\n")


    def createMultMatrix(self):
        # Get the unique ids
        ids = self.edgelist[["target_id", "origin_id"]].stack().reset_index(drop=True)
        ids.drop_duplicates(inplace=True)

        # Create a DF to map the values and set all values to 0
        self.multDF = pd.DataFrame(columns=ids.to_list(), index=ids.to_list())
        for col in self.multDF.columns:
            self.multDF[col].values[:] = 0

        # Loop through the edgelist and set the values in the DF
        print("Creating the multiplication matrix ...")
        for i, row in self.edgelist.iterrows():

            target_id = row["target_id"]
            origin_id = row["origin_id"]
            premises = row["num_premises"]
            cos_sim = row["cos_sim"]

            # Set the factor in the correct column and row
            self.multDF[origin_id].loc[[target_id]] = cos_sim*1/premises

        # Turn the DF into a numpy array, multiply by alpha and save it in the multMatrix
        self.multMatrix = (self.alpha * self.multDF.to_numpy()).astype(np.float64)


    def calculateArgRank(self):
        print("="*50)
        print("Calculating the ArgRank iteratively...")

        # Initialize the vector of ArgRanks with 1's
        argRankVector = np.full(shape=self.multMatrix.shape[0],
                                fill_value=1).astype(np.float64)

        # Allocate GPU memory for the matrix
        multMatrix_gpu = gpuarray.to_gpu(np.ascontiguousarray(self.multMatrix))

        # Loop until the ArgRanks converge
        converged = False
        iteration = 1
        while not converged:
            # Allocate GPU memory for the vector and calculate the new Vector
            argRankVector_gpu = gpuarray.to_gpu(np.ascontiguousarray(argRankVector))
            newVec = linalg.dot(multMatrix_gpu, argRankVector_gpu).get() + self.groundRelevance

            max_diff = np.max(np.abs(newVec - argRankVector))
            print("- Iteration {}, max difference: {}".format(iteration, max_diff))

            if max_diff < self.convergDiff:
                converged = True

            argRankVector = newVec
            iteration += 1

            if iteration > 1000:
                break

        # Store the final argRanks in the multDF
        self.multDF["argRank"] = argRankVector
        print("ArgRank-values calculated.")
        print("=" * 50 + "\n")


    def saveArgRank(self):
        print("=" * 50)
        print("Saving ArgRanks to {} ...".format(self.outpath))

        # Create a column with ground relevance for the ArgRank in the argDF
        self.argDF["argRank"] = self.groundRelevance

        # Update the argRank-column based on the multDF
        i = 1
        total = len(self.multDF)
        for arg_id, row in self.multDF.iterrows():
            argRank = row["argRank"]
            self.argDF.loc[self.argDF["id"] == arg_id, "argRank"] = argRank

            print("- Mapped {}/{} argRanks".format(i, total))
            i += 1

        # Save the updated argDF as CSV
        self.argDF.to_csv(self.outpath)
        print("ArgRanks saved in {}.".format(self.outpath))
        print("=" * 50 + "\n")



    def readArgsCSV(self, chunksize=10000):
        """
        Function that reads in the argument body to be used to create the arg graph
        :param chunksize:   The number of lines to be read in from the CSV at once
        """
        self.argDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.arg_path, chunksize=chunksize)
        for i, chunk in enumerate(rows):
            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.argDF = chunk
            else:
                self.argDF = pd.concat([self.argDF, chunk], axis=0)


    def defineOutpath(self,
                      edgelist_path,
                      min_cosine_sim,
                      cosine_weight):

        # Get necessary parts of the edgelist_path
        str1 = "Edgelist_"
        str2 = ".csv"
        interim = edgelist_path[edgelist_path.find(str1)+len(str1): edgelist_path.find(str2)]
        source = interim[: interim.find("_")]

        cos_sim = str(int(min_cosine_sim*100))
        alpha = str(int(self.alpha*100))

        # Save information on whether cosine weighting was used and if base is below 1
        if cosine_weight:
            addition = "_CosWeighted"
        else:
            addition = ""

        if not self.groundRelBool:
            addition = addition + ""
        else:
            addition = addition + "_groundRel"

        self.outpath = OUT_DIR + "argsAR_" + source + "_minSim" + cos_sim + addition + "_" + alpha + ".csv"


# Define the different versions of the ArgRank
# - Number 1-8:     Similarity Thresholds between 0.7 and 0.95 with no cosine weighting
#       - Numbers 1-4:  Ground relevance = 1 with alpha=0.15
#       - Number 5-8:   Ground relevance calculated with alpha=0.85

# - Number 9:       Cosine weighting for all edges with similarity > 0.7; groundRel=1, alpha=0.15

min_sims = [0.75, 0.8, 0.85, 0.9]
alphaValues = [0.2, 0.3]
cosWeighting = [False, True]


# Create the different ArgRanks
for sim in  min_sims:
    print("\n"+"="*50)
    print("Creating ArgRanks for Cosine " + str(sim))
    print("=" * 50 + "\n")

    for alpha in alphaValues:
        for cos in cosWeighting:

            argRanker = ArgRanker(min_cosine_sim=sim,
                                  alpha=alpha,
                                  cosine_weight=cos)

            argRanker.calculateArgRank()
            argRanker.saveArgRank()

#graphBuilder = ArgGraphBuilder(search_space="retrieval")
#graphBuilder.createArgGraphs()