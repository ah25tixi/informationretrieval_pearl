from sklearn.metrics.pairwise import cosine_similarity, manhattan_distances
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import preprocessing
from sentence_transformers import SentenceTransformer
from transformers import BertForNextSentencePrediction, BertTokenizer
import math
import itertools
from torch import LongTensor

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import numpy as np

# Download the stopwords and punkt
#nltk.download('stopwords')
#nltk.download('punkt')

class SentenceMatcher:
    def __init__(self, mode="neighbour", mmrWeight=0.5):
        """
        :param mode:        Define which type of matching will be used
        :param mmrWeight:   Weight used for the positive similarity in MMR weighting
                            The negative similarity (cos_sim) is weighted using (1-mmrWeight)
        """

        if mode == "neighbour":
            pass

        else:
            self.mmrWeight = mmrWeight
            self.model = SentenceTransformer('all-mpnet-base-v2')
            self.model_nsp = BertForNextSentencePrediction.from_pretrained("bert-base-uncased")
            self.tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
            self.vectorizer = CountVectorizer()

        self.stemmer = PorterStemmer()
        self.stop_words = set(stopwords.words('english'))
        print("- Model for Sentence Matching loaded")

    # ==========================================================================
    # Neighbour-Matching
    # ==========================================================================
    def matchingNeighbour(self, senDF):
        # Create a DF to store the results (deep copy)
        resultDF = senDF.copy(deep=True)

        # Get the ids as a list
        id_list = senDF["docno"].to_list()

        # Create a list of used ids to avoid duplicate sentences
        used_id_list = []

        # Loop until all sentences in senDF are processed
        while len(id_list) > 0:
            # As the id_list is constantly updated, the first element is always the next id
            part_one_id = id_list[0]
            rowfilter = senDF["docno"] == part_one_id
            part_two_id = senDF.loc[rowfilter, "partner"].to_numpy()[0]

            # Check if one of the ids was already used. If so, remove it from the DF and list and continue
            if part_two_id in used_id_list or part_two_id in used_id_list:
                part_one_index = senDF.index[senDF['docno'] == part_one_id].tolist()
                senDF.drop(part_one_index, inplace=True)
                id_list.pop(0)
                continue

            # Otherwise, get the relevant information
            part_one_sen = senDF.loc[rowfilter, "sentence"].to_numpy()[0]
            part_two_sen = senDF.loc[rowfilter, "partner_text"].to_numpy()[0]
            partner_position = senDF.loc[rowfilter, "partner_position"].to_numpy()[0]

            # Set the order of the sentences based on the position of the partner (before or after)
            # - If the partner comes before the sentence, the positions are reversed
            rowfilter = resultDF['docno'] == part_one_id
            if partner_position == "after":
                resultDF.loc[rowfilter, 'pair_id'] = part_two_id
                resultDF.loc[rowfilter, 'pair_sen'] = part_two_sen
            else:
                resultDF.loc[rowfilter, 'pair_id'] = part_one_id
                resultDF.loc[rowfilter, 'pair_sen'] = part_one_sen
                resultDF.loc[rowfilter, 'sentence'] = part_two_sen
                resultDF.loc[rowfilter, 'docno'] = part_two_id

            # Update the senDF and id list by removing the chosen elements
            # Always remove 2nd before 1st to avoid mismatching
            part_two_index = senDF.index[senDF['docno'] == part_two_id].tolist()
            part_one_index = senDF.index[senDF['docno'] == part_one_id].tolist()
            senDF.drop(part_two_index, inplace=True)
            senDF.drop(part_one_index, inplace=True)

            # Check if the partner id is in the id_list and remove it
            if part_two_id in id_list:
                idx = id_list.index(part_two_id)
                id_list.pop(idx)

            # Remove the processed id
            id_list.pop(0)

            # Update the list of used ids
            used_id_list.append(part_one_id)
            used_id_list.append(part_two_id)

        # Return all complete rows of the DF (Drop NaN rows)
        return resultDF.dropna()

    # ==========================================================================
    # MMR-Matching
    # ==========================================================================

    # Function that matches using Maximal Marginal Relevance (MMR)
    def matchingMMR(self, senDF):
        # Create a DF to store the results (deep copy)
        resultDF = senDF.copy(deep=True)

        # Get list of sentences and ids
        sentence_list = senDF["sentence"].to_list()
        id_list = senDF["docno"].to_list()

        # Calculate the cosine similarities between sentences
        self.sen_encodings = self.model.encode(sentence_list)
        cosine = cosine_similarity(np.array(self.sen_encodings))

        # Calculate the next sentence probabilities between all sentences
        nspProb = self.nsp_probabilities_looped(sentence_list)

        # Loop over the rows of senDF to create matches (Used sentences are removed)
        part_one_pos = 0
        while len(id_list) >= 2:
            # Get the id of the next sentence
            part_one_id = id_list[part_one_pos]

            # Calculate MMR based on two inputs: argument next sentence probability and cosine similarity
            sim1 = nspProb[part_one_pos, :]
            sim2 = cosine[part_one_pos, :]
            mmr = self.mmrWeight*sim1 - (1-self.mmrWeight)*sim2

            # Get list index of highest MMR with different ID (If possible, with same stance)
            part_two_pos = self.getConditionalMmrIndex(senDF=senDF, mmr=mmr, part_one_id=part_one_id)

            # Get the pair elements and remove them from the lists
            part_two_id = id_list.pop(part_two_pos)
            part_two_sen = sentence_list.pop(part_two_pos)

            # Save the id and sentence into the resultDF
            resultDF.loc[resultDF['docno'] == part_one_id, 'pair_id'] = part_two_id
            resultDF.loc[resultDF['docno'] == part_one_id, 'pair_sen'] = part_two_sen

            # Update the senDF, cosine matrix, nspPrb matrix, and both lists by removing the chosen elements
            # Always remove 2nd before 1st to avoid mismatching
            part_two_index = senDF.index[senDF['docno'] == part_two_id].tolist()
            part_one_index = senDF.index[senDF['docno'] == part_one_id].tolist()

            senDF.drop(part_two_index, inplace=True)
            senDF.drop(part_one_index, inplace=True)

            for i in [0, 1]:
                cosine = np.delete(cosine, (part_two_pos), axis=i)
                cosine = np.delete(cosine, (part_one_pos), axis=i)
                nspProb = np.delete(nspProb, (part_two_pos), axis=i)
                nspProb = np.delete(nspProb, (part_one_pos), axis=i)

            sentence_list.pop(part_one_pos)
            id_list.pop(part_one_pos)

        # Return all complete rows of the DF (Drop NaN rows)
        return resultDF.dropna()

    # Separate function to calculate the MMR score
    def calculateMMR(self, scores, cosine, part_one_pos, positive_weight):
        # Normalize the relevance scores and cosine similarity
        norm = np.linalg.norm(scores)
        sim1 = scores / norm
        sim2 = cosine[part_one_pos, :]

        # Calculate the MMR and return it
        return positive_weight * sim1 - (1 - positive_weight) * sim2

    # Function that returns the position of the sentence with the highest MMR conditional on stance and non-duplicates
    def getConditionalMmrIndex(self, senDF, mmr, part_one_id):
        # Get the stance of the main argument
        stance = senDF.loc[senDF["docno"] == part_one_id]["arg_stance"].values[0]

        # Filter the senDF based on stance and part_one_id and get the maximum value in the MMR column
        senDF["MMR"] = mmr
        max_val = senDF.loc[((senDF["arg_stance"] == stance) & (senDF["docno"] != part_one_id))]["MMR"].max()

        # Check if a value was found, otherwise match without stance
        if math.isnan(max_val):
            max_val = senDF.loc[senDF["docno"] != part_one_id]["MMR"].max()

        # Return the position of the max value
        return mmr.tolist().index(max_val)

    # Function to calculate a matrix of next sentence probabilities for a list of sentences
    def nsp_probabilities(self, sen_list):
        # Create a matrix to store the results (a column-vector is calculated for each sentence)
        length = len(sen_list)
        result = np.zeros((length, length))

        # Calculate the individual sentence vectors and store them in the result matrix
        for j, sen in enumerate(sen_list):
            print("Processing sentence {}/{}".format(j + 1, length))

            # Calculate the tokens for the current sentence paired with all other sentences
            sen_repeated = list(itertools.repeat(sen, length))
            tokens = self.tokenizer(sen_repeated, sen_list, return_tensors='pt', padding=True)

            # Calculate the scores for the tokens and save them as a column of the result matrix
            result[:, j] = self.model_nsp(**tokens).logits[:, 0].detach().numpy()

        # Fill the diagonal with 0's as that would mean matching on the same sentence
        np.fill_diagonal(result, 0)

        # Return the normalized matrix
        return preprocessing.normalize(result)

    # Storage efficient version of nsp_probabilities (higher runtime complexity)
    def nsp_probabilities_looped(self, sen_list):
        nsp_df = []
        length = len(sen_list)

        for j, el in enumerate(sen_list):
            print("Processing sentence {}/{}".format(j + 1, length))
            my_tokenizer_lst = [self.tokenizer(i, text_pair=el, return_tensors="pt") for i in sen_list]
            my_output_lst = []
            [my_output_lst.append(float(self.model_nsp(**i, labels=LongTensor([1])).logits[0, 0])) for i in
             my_tokenizer_lst]
            nsp_df.append(my_output_lst)

        nsp_df = np.array(nsp_df)

        # Fill the diagonal with 0's as that would mean matching on the same sentence
        np.fill_diagonal(nsp_df, 0)

        # Return the normalized matrix
        return preprocessing.normalize(nsp_df)

    # ==========================================================================
    # Other forms of matching and helper functions
    # ==========================================================================

    # Function that creates sentence pairs based on the sentence similarity
    def matchingOnSentences(self, senDF):
        # Get a list of sentences and ids
        sentence_list = senDF["sentence"].to_list()
        id_list = senDF["docno"].to_list()

        self.sen_encodings = self.model.encode(sentence_list)

        # Get the best matches
        cosine = self.adjustedCosine(self.sen_encodings)
        best_matches = np.argmax(cosine, axis=0)

        # Save the pairs in the senDF (matchIndex is used as the senDF has a different index structure)
        matchIndex = 0
        for j, _ in senDF.iterrows():
            senDF.loc[j, 'pair_sen'] = sentence_list[best_matches[matchIndex]]
            senDF.loc[j, 'pair_id'] = id_list[best_matches[matchIndex]]

            matchIndex += 1

        return senDF

    # Function that creates sentence pairs based on the weighted similarity between args and sentences
    def matchingOnArgAndSen(self, senDF, senWeight=0.8):
        # Get lists of the sentences and of the arguments
        sentence_list = senDF["sentence"].to_list()
        arg_list = senDF['argument'].to_list()
        id_list = senDF["docno"].to_list()

        # Encode the sentences and apply stemming to the arguments
        self.sen_encodings = self.model.encode(sentence_list)
        arg_list = self.stopWordsAndStemming(arg_list)

        # Get the adjusted cos sim of the sentences and the similarities for stemmed-args based on manhattan distance
        sen_cosine = self.adjustedCosine(self.sen_encodings)
        arg_sim = self.wordDistances(arg_list)

        # Add both scores
        similarity = senWeight*sen_cosine + (1-senWeight)*arg_sim

        # Get the best match
        best_matches = np.argmax(similarity, axis=0)

        # Save the pairs in the senDF (matchIndex is used as the senDF has a different index structure)
        matchIndex = 0
        for j, _ in senDF.iterrows():
            senDF.loc[j, 'pair_sen'] = sentence_list[best_matches[matchIndex]]
            senDF.loc[j, 'pair_id'] = id_list[best_matches[matchIndex]]

            matchIndex += 1

        return senDF

    def adjustedCosine(self, encodings):
        # Calculate the encodings and cosine similarity between them
        # - To avoid having pairs of the same sentence, very high similarities are set to 0
        cosine = cosine_similarity(np.array(encodings))
        cosine[cosine >= 0.99] = 0

        return cosine

    # Function that calculates the manhattan distance between strings and returns them in a similarity format
    # New format: 0 = No similarity, 1 = Exact Match
    def wordDistances(self, stringList, gamma=0.001):
        # Get the one hot vectors for each string from the vectorizer
        one_hot_vectors = self.vectorizer.fit_transform(stringList).toarray()

        # Calculate the manhattan distances between vectors
        manhattan = manhattan_distances(one_hot_vectors)

        # Turn the distances into similarity scores with an RBF-Kernel
        # https://stats.stackexchange.com/questions/158279/how-i-can-convert-distance-euclidean-to-similarity-score
        # - Parameter-free alternative: 1/(1+manhattan) [Leads to very low values]
        similarity = np.exp(-gamma*np.square(manhattan))

        return similarity

    # Function to remove stopwords and apply stemming
    def stopWordsAndStemming(self, stringList):
        # Loop over all strings in the list and remove stopwords
        for i, str in enumerate(stringList):
            word_tokens = word_tokenize(str)
            filtered_sen = [w for w in word_tokens if not w.lower() in self.stop_words]

            # Stemm all remaining tokens
            word_list = []
            for word in filtered_sen:
                word_list.append(self.stemmer.stem(word))

            # Merge the words again and save them in the stringList
            stringList[i] = " ".join(word_list)

        return stringList