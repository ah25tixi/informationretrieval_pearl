import pandas as pd
import seaborn as sns
import math

df1 = pd.read_csv('Retrieval_results_v2_MMR_top100_reduced_Alex.csv')
df2 = pd.read_csv('Retrieval_results_v2_MMR_top100_reduced_Basti.csv',
                  usecols=['Argumentativeness', 'Sentence_Coherence', 'Represents_Argument'])
df2.columns = ['Argumentativeness 2', 'Sentence_Coherence 2', 'Represents_Argument 2']
df3 = pd.read_csv('Retrieval_results_v2_MMR_top100_reduced_Bianca.csv',
                  usecols=['Argumentativeness', 'Sentence_Coherence', 'Represents_Argument'])
df3.columns = ['Argumentativeness 3', 'Sentence_Coherence 3', 'Represents_Argument 3']
df4 = pd.read_csv('Retrieval_results_v2_MMR_top100_reduced_jonas.csv',
                  usecols=['Argumentativeness', 'Sentence_Coherence', 'Represents_Argument'])
df4.columns = ['Argumentativeness 4', 'Sentence_Coherence 4', 'Represents_Argument 4']
concat = pd.concat([df1, df2, df3, df4], axis=1)

concat['Argumentativeness mean'] = concat[['Argumentativeness', 'Argumentativeness 2', 'Argumentativeness 3', 'Argumentativeness 4']].mean(axis=1)
concat['Sentence_Coherence mean'] = concat[['Sentence_Coherence', 'Sentence_Coherence 2', 'Sentence_Coherence 3', 'Sentence_Coherence 4']].mean(axis=1)
concat['Represents_Argument mean'] = concat[['Represents_Argument', 'Represents_Argument 2', 'Represents_Argument 3', 'Represents_Argument 4']].mean(axis=1)

a = concat[['Argumentativeness', 'Argumentativeness 2', 'Argumentativeness 3', 'Argumentativeness 4']]
concat['Argumentativeness diff'] = abs(a.max(axis=1) - a.min(axis=1))
b = concat[['Sentence_Coherence', 'Sentence_Coherence 2', 'Sentence_Coherence 3', 'Sentence_Coherence 4']]
concat['Sentence_Coherence diff'] = abs(b.max(axis=1) - b.min(axis=1))
c = concat[['Represents_Argument', 'Represents_Argument 2', 'Represents_Argument 3', 'Represents_Argument 4']]
concat['Represents_Argument diff'] = abs(c.max(axis=1) - c.min(axis=1))

concat['3d_evaluation'] = concat[['Argumentativeness mean', 'Sentence_Coherence mean', 'Represents_Argument mean']].mean(axis=1)
concat.to_csv('analysis_output/Partner_evaluation.csv')

D_evaluation = []
argumentativness = []
Sentence_Coherence = []
Represents_Argument = []
precision = []
dcg = []
for i in range(1, 51):
    zw = concat[concat['query_num'] == i]
    a = zw['Argumentativeness mean'].where(round(zw['Argumentativeness mean']) > 0).count()
    b = len(zw) - a
    D_evaluation.append(zw['3d_evaluation'].mean())
    argumentativness.append(zw['Argumentativeness mean'].mean())
    Sentence_Coherence.append(zw['Sentence_Coherence mean'].mean())
    Represents_Argument.append(zw['Represents_Argument mean'].mean())
    precision.append(a/(a-b))
    sum = 0
    sum2 = 0
    for j in enumerate(round(zw['Argumentativeness mean'])):
        sum += (2 ** j[1] - 1) / math.log2(1 + (j[0] + 1))
        sum2 += 2 ** j[1] - 1
    dcg.append(sum)


graph = pd.DataFrame(data={'d_evaluation': D_evaluation,
                           'Argumentativeness': argumentativness,
                           'Sentence_Coherence': Sentence_Coherence,
                           'Represents_Argument': Represents_Argument,
                           'Precision': precision,
                           'DCG': dcg},
                     index=range(1, 51))
graph = graph.drop([1, 2, 4, 5, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 38, 40, 41, 42, 43, 44, 46, 50])
graph.to_csv('analysis_output/Partner_graph.csv')

sns.set(rc={'figure.figsize': (20, 10)})
sns.barplot(y=graph.d_evaluation, x=graph.index).set_title('3d_evaluation Partner').figure.savefig('analysis_output/Partner_3d_evaluation.png')
sns.barplot(y=graph.Argumentativeness, x=graph.index).set_title('Argumentativeness Partner').figure.savefig('analysis_output/Partner_argumentativeness.png')
sns.barplot(y=graph.Sentence_Coherence, x=graph.index).set_title('Sentence Coherence Partner').figure.savefig('analysis_output/Partner_SentenceCoherence.png')
sns.barplot(y=graph.Represents_Argument, x=graph.index).set_title('Represents_Argument Partner').figure.savefig('analysis_output/Partner_RepresentArgument.png')
sns.barplot(y=graph.Precision, x=graph.index).set_title('Precision Partner').figure.savefig('analysis_output/Partner_Precision.png')
sns.barplot(y=graph.DCG, x=graph.index).set_title('DCG Partner').figure.savefig('analysis_output/Partner_DCG.png')

graph.plot(y=["Argumentativeness", "Sentence_Coherence", "Represents_Argument"], kind="bar", figsize=(9, 8)).figure.savefig('analysis_output/Partner_barplot_com.png')
