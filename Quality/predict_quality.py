import numpy as np
import pandas as pd
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from transformers import AutoModel, AutoTokenizer
import torch.nn.functional as F
from classes import CustomBERTModel, PredictionDataset

device = 'cuda'
model = CustomBERTModel().to(device)
model.load_state_dict(torch.load('./quality_prediction_baseline.pth'))
model.eval()
# execute quality prediction on args.me corpus, split in multiple chunks and preprocessed to extract premises
for part in range(1, 38):
    df_in = pd.read_csv('../Indexing/args_corpus/args_processed_part_{}_out.csv'.format(part))
    text = df_in['premises'].tolist()
    token_length = 512

    batch_size = 32
    results = []
    tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", model_max_length=token_length)
    encodings = tokenizer(text, truncation=True, padding=True, return_tensors="pt")

    dataset = PredictionDataset(encodings)
    dataloader = DataLoader(dataset, batch_size=batch_size)

    with tqdm(dataloader, unit="batch", position=0, leave=True) as tepoch:
        for i, (ids, mask) in enumerate(tepoch):
            tepoch.set_description(f"Part {part}")
            with torch.no_grad():
                results.append(model(ids.to(device), mask.to(device)).tolist())
    df_out = df_in[['id', 'premises']]
    df_out['quality'] = np.concatenate(results).flatten()
    df_out.to_csv('../Quality/out/args_processed_part_{}_quality.csv'.format(part))
