Argument and Sentences Collection
================================================

### Argument Files
The files args.csv, argsArgRank_75_30.csv, argsArgRank_80_40.csv contain the processed collection of arguments. The latter two contain an additional column "argRank" that has the individual ArgRank-scores for each argument under the specified parameters (75_30: Minimum cosine similarity = 0.75 and alpha = 0.3).

The shared columns are:
<br><br/>

|Column            | Description                                                                 |
|:-----------------|:----------------------------------------------------------------------------|
|id                |The argument's id in the args.me corpus                                      |
|conclusion        |The argument's conclusion                                                    |
|premises          |The argument's premises                                                      |
|stance            |The stance of the argument to the discussing title                           |
|discussion_title  |The title of the discussion the argument was voice in                        |
|quality           |The quality of the argument as estimated by the approach in [predict_quality.py](../../Quality/predict_quality.py)  |


### Sentences Files
The sentences.csv contains the full sentence collection, while the sentences_slim.csv is a smaller version used in retrieval to reduce loading time. 

The columns are:
<br><br/>

|Column             | Description                                                                 |
|:------------------|:----------------------------------------------------------------------------|
|arg_ids            |The id of the parent argument in the args.me corpus                          |
|sen_id             |The id of the sentence in the args.me corpus                                 |         |sentence           |The sentence itself                                                          |
|conclusion         |The conclusion of the parent argument                                        |
|stance             |The stance of the parent argument                                            |
|quality            |The quality of the parent argument: [predict_quality.py](../../Quality/predict_quality.py)                                                                               |
|sent_quality       |The quality of the sentence: [sentence_quality.py](../../Quality/sentence_quality.py)                                                                              |
|partner            |The id of the optimal neighbour (used in sentence matching)                  |
|partner_position   |The position of the neighbour in the parent argument (before or after the sentence)                                                                                         |
|partner_quality    |The quality of the resulting sentence pair: [sentence_quality.py](../../Quality/sentence_quality.py)                                                                              |
