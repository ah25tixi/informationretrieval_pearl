FROM jupyter/base-notebook:latest
  
USER root


RUN apt-get update
RUN apt-get install -y python3-pip
RUN apt-get install -y openjdk-11-jdk

RUN pip install python-terrier

RUN pip install pandas vaderSentiment sentence_transformers nltk numpy scikit_learn transformers


EXPOSE 8888

CMD ["jupyter", "notebook", "--port=8888", "--allow-root","--no-browser", "--ip=0.0.0.0"]
