import numpy as np
import pandas as pd
import CSV_split as split
import index as ind
import retrieve as ret
import xml.etree.ElementTree as ET
from Utilities.Sentiment_Analysis.sentiment_analysis import SentimentAnalyzer
from Utilities.sentence_similarity import SentenceMatcher
from os.path import exists

# Don't show warnings about chained assignments
pd.options.mode.chained_assignment = None

class PearlRetriever:
    """
    Main retriever that combines the classes in retrieve.py
    """

    def __init__(self,
                 argsPath,
                 senPath,
                 argsModel="DirichletLM",
                 senModel="DPH",
                 argRank=False,
                 matching="neighbour",
                 numArgs=1000,
                 numSen=5000,
                 numPairingCandidates=500,
                 numPairs=10,
                 minArgQuality=1,
                 minSentQuality=0.7,
                 weightedScore=True,
                 mmrWeight=0.5,
                 blockFile = "Evaluation/blockword_list.txt"):

        """
        :param argsPath:                Path to the processed argument CSV
        :param senPath:                 Path to the processed sentences CSV
        :param argsModel:               Retrieval model for the arguments
        :param senModel:                Retrieval model for the sentences
        :param argRank:                 Boolean to state if ArgRanks should be used in ranking
        :param matching:                'neighbour': Match sentences with best preceding/suceeding sentence in their arg
                                        Other values: Use MMR-matching (Highest next sentences and lowest cosine sim)
        :param numArgs:                 Number of arguments to be retrieved
        :param numSen:                  Number of sentences to be retrieved
        :param numPairingCandidates:    Number of sentences to form pairs from
        :param numPairs:                Number of pairs to be retrieved
        :param minArgQuality:           Minimum quality scores of arguments to be retrieved
        :param minSentQuality:          Minimum quality scores of sentences to be retrieved
        :param weightedScore:           True: Rank retrieval results based on weighted sum arg-score and sen-score
                                        False: Rank by arg_score -> score (sen-score)
        :param mmrWeight:               Weight factor for the Next Sentence Prediction in MMR-Matching
        :param blockFile:               Textfile containing words based on which retrieval results are discarded
        """

        # Path variables
        self.argsPath = argsPath
        self.senPath = senPath

        # Variables for the argument retriever
        self.argsModel = argsModel
        self.numArgs = numArgs

        # Variables for the sentences retriever
        self.senModel = senModel
        self.numSen = numSen

        # Blockword list for retrieved sentences
        with open(blockFile, 'r') as file:
            self.blockwords = file.read().split('\n')[:-1]

        # Further parameters
        self.minArgQuality = minArgQuality
        self.minSentQuality = minSentQuality
        self.weightedScore = weightedScore
        self.numPairingCandidates = numPairingCandidates
        self.numPairs = numPairs
        self.argRank = argRank
        self.matching = matching

        # Prepare the retrievers
        print("-"*50)
        print("Preparing retrievers and loading text-dataframes...")
        self.argsRetriever = ret.ArgsRetriever(arg_path=self.argsPath,
                                               modeltype=self.argsModel,
                                               num_results=self.numArgs)

        self.senRetriever = ret.SentencesRetriever(sen_path=self.senPath,
                                                   modeltype=self.senModel,
                                                   num_results=self.numSen)

        # Prepare the sentence matcher
        print("\nPreparing the sentence matcher...")
        self.sentenceMatcher = SentenceMatcher(mode=matching, mmrWeight=mmrWeight)

        print("\nPreparation completed.")
        print("-" * 50 + "\n")


    # Retrieval to .txt-file in the TREC Format
    def retrieveToTxtTREC(self, topicXML, filepath, method=""):
        """
        :param topicXML:        Path to the XML-file containing the queries for retrieval
        :param filepath         Path to which the results are written
        :param method:          Name of the method used in retrieval
        """

        # Initialize the SentimentAnalyzer (checks if query and discussion titles have the same sentiment)
        senAnalyzer = SentimentAnalyzer()

        # Perform retrieval for each of the queries in the topicXML
        topics = ET.parse(topicXML).getroot()
        for t in topics:
            num = t.find("number").text
            query = t.find("title").text

            # Perform the actual retrieval
            print("Processing query: " + query)
            self.retrieve(query=query)
            senAnalyzer.setQueryText(query)

            # Get the top pairs and give them rank numbers
            topRows = self.formSentencePairs()
            topRows['rank'] = np.arange((topRows.shape[0]))

            # Write each of the sentence pairs into a line
            for j, row in topRows.iterrows():
                self.writeTRECLine(query_num=num,
                                   method=method,
                                   outpath=filepath,
                                   row=row,
                                   senAnalyzer=senAnalyzer)


    # Retrieval to csv used in evaluation
    def retrieveToCSV(self, queryset, filepath="Retrieval_results.csv"):
        """
        :param queryset:        Set of strings containing the queries
        :param filepath:        The path of the retrieval results
        """

        # Check if a file already exists at the path
        new_content = True
        # Create a file with header (if non-existent)
        if exists(filepath):
            new_content = False
        else:
            with open(filepath, "w") as outf:
                # Write header
                header = 'query_num,query,sen_id,arg_score,sen_score,pair_id,sentence_pair,argument,stance,disc_title,stance_to_query\n'
                outf.write(header)
                outf.close()

        # If a file was found, get the last processed query
        last_query = ""
        if not new_content:
            print("An existing retrieval-file with the same name was found. Only new queries will be processed.")
            last_query = self.getLastQuery(filepath)

        # Initialize the SentimentAnalyzer (checks if query and discussion titles have the same sentiment)
        senAnalyzer = SentimentAnalyzer()

        # Perform retrieval for each of the queries in the queryset
        for i, query in enumerate(queryset):
            # Check if the query is the last that was processed to set new_content to true
            # If not, pass this query
            if not new_content:
                if query == last_query:
                    new_content = True
                    print("-" * 50)
                    print("Starting to process new queries")
                    print("-" * 50)
                continue

            # Perform the actual retrieval
            print("Processing query: " + query)
            self.retrieve(query=query)
            senAnalyzer.setQueryText(query)

            # Get the top pairs
            topRows = self.formSentencePairs()

            # Write each of the sentence pairs into a line
            for j, row in topRows.iterrows():
                self.writeLine(query_num=i,
                               outpath=filepath,
                               query=query,
                               row=row,
                               senAnalyzer=senAnalyzer)



    #========================================================================================
    #      Retrieval
    #========================================================================================
    # Function that conducts the retrieval for a single query
    def retrieve(self, query):
        # Perform the retrieval
        self.retrieveArgs(query)
        self.retrieveSentences(query)

        # Restrict results
        # - Arguments below minArgQuality are discarded
        # - Sentences from other arguments then the pre-selected ones are discarded
        # - Sentences below the minSentQuality are discarded
        self.retrievedArgs = self.retrievedArgs.loc[self.retrievedArgs['quality'] < self.minArgQuality]
        self.retrievedSen = self.retrievedSen.loc[self.retrievedSen["args_id"].isin(self.retrievedArgs["docno"])]
        self.retrievedSen = self.retrievedSen.loc[self.retrievedSen["sent_quality"] < self.minSentQuality]
        self.retrievedSen['sentence_lower'] = self.senRetriever.matchSenStringsToDocnoDF(self.retrievedSen)["sentence"].str.lower()
        for word in self.blockwords:
            self.retrievedSen = self.retrievedSen[~self.retrievedSen.sentence_lower.str.contains(word)]
        self.retrievedSen = self.retrievedSen.drop(columns=['sentence_lower'])

        # If ArgRanks should be included in ranking, adapt the arg_score
        if self.argRank:
            self.argRankReranking()

        # Get meta data from the arguments (score=Argument Relevance, discussion_title, argRank)
        for meta_data in ["score", "discussion_title"]:
            self.retrievedSen = self.appendArgMetaDataToSenDF(argsDF=self.retrievedArgs,
                                                              senDF=self.retrievedSen,
                                                              args_column=meta_data)

        # Get the ranked retrieval result
        self.getRankedResults()

    # Function to obtain arguments for a query
    def retrieveArgs(self, query):
        if not hasattr(self, "argsRetriever"):
            self.argsRetriever = ret.ArgsRetriever(modeltype=self.argsModel,
                                                   arg_path=self.argsPath,
                                                   num_results=self.numArgs)

        self.argsRetriever.retrieve(query)

        # Save the retrieved arguments
        self.retrievedArgs = self.argsRetriever.retrievalDF

    # Function to obtain sentences for a query
    def retrieveSentences(self, query):
        if not hasattr(self, "senRetriever"):
            self.senRetriever = ret.SentencesRetriever(modeltype=self.senModel,
                                                       sen_path=self.senPath,
                                                       num_results=self.numSen)

        self.senRetriever.retrieve(query)

        # Save the retrieved sentences
        self.retrievedSen = self.senRetriever.retrievalDF



    #========================================================================================
    #      Pairing and Ranking
    #========================================================================================

    # Function that takes the current retrieval results and forms pairs from them
    def formSentencePairs(self):
        # Get the top sentences (# = numPairingCandidates) and obtain necessary information
        pairingCandidates = self.retrievalResult.iloc[:self.numPairingCandidates]
        pairingCandidates = self.appendInformationToRetrievalDF(pairingCandidates)

        # Remove duplicate sentences and form pairs
        pairingCandidates.drop_duplicates(subset=["docno"], inplace=True)
        pairingCandidates.drop_duplicates(subset=["sentence"], inplace=True)

        if self.matching == "neighbour":
            # Get the texts to all neighbour sentences based on their id's
            partnerIds = pairingCandidates[["partner", "partner_position"]]
            partnerIds.rename(columns={'partner': 'docno'}, inplace=True)
            pairingCandidates["partner_text"] = self.senRetriever.matchSenStringsToDocnoDF(partnerIds)["sentence"]

            # Form the pairs
            pairs = self.sentenceMatcher.matchingNeighbour(pairingCandidates)

        else:
            pairs = self.sentenceMatcher.matchingMMR(pairingCandidates)

        # Return the top pairs
        return pairs.iloc[:self.numPairs]


    # Function to rerank the retrieval result and set the attribute "retrievalResult"
    # - If weightedScore = True -> weighted ranking (Parameters depend on use of argRank)
    # - Else: Rank by arg_score -> score (sen-score)
    def getRankedResults(self):
        if not self.weightedScore:
            # Rerank the sentences based on the (i) relevance of the arguments (ii) their relevance score
            # - This is combined in a single value by multiplying the arg_score with 100 and adding the sen_score
            self.retrievedSen['finalScore'] = (100*self.retrievedSen.arg_score + self.retrievedSen.score)/100

        else:
            senWeight = 0.51
            if self.argRank:
                senWeight = 0.38

            self.retrievedSen["finalScore"] = (1-senWeight) * self.retrievedSen.arg_score + \
                                                 senWeight * self.retrievedSen.score

        # Sort results by the final score (Ties are handled by (i) sentence score and (ii) sentence quality)
        self.retrievalResult = self.retrievedSen.sort_values(['finalScore', 'score', 'sent_quality'],
                                                             ascending=[False, False, False])

    # Function to rerank using the argument Rank
    def argRankReranking(self):
        relevantArgs = self.argsRetriever.argDF.loc[self.argsRetriever.argDF["docno"].isin(self.retrievedArgs["docno"])]

        # Update the values in self.retrievedArgs (ArgRanks are multiplied by 300.000 to have scores in a useful range)
        for i, row in relevantArgs.iterrows():
            argID = row["docno"]
            argRank = row["argRank"] * 300000
            score = self.retrievedArgs.loc[self.retrievedArgs["docno"] == argID, "score"]

            self.retrievedArgs.loc[self.retrievedArgs["docno"] == argID, "score"] = score*argRank


    #========================================================================================
    #      Appending Information and Writing Results
    #========================================================================================

    # Append additional information to a DF of sentences IDs
    def appendInformationToRetrievalDF(self, retrievalDF):
        # Get sentence and conclusion text from the senDF of the senRetriever
        retrievalDF = self.senRetriever.matchSenStringsToDocnoDF(retrievalDF)

        # Get argument texts from the argDF of the argsRetriever
        retrievalDF = self.argsRetriever.matchArgStringsToDocnoDF(retrievalDF)

        # Get stance from the retrieved args
        retrievalDF = self.appendArgMetaDataToSenDF(argsDF=self.retrievedArgs,
                                                    senDF=retrievalDF,
                                                    args_column="stance")
        return retrievalDF

    # Function that adds meta data from the retrieved arguments to a DF of sentences
    def appendArgMetaDataToSenDF(self, argsDF, senDF, args_column):
        """
        :param argsDF:          DataFrame of retrieved arguments with meta data
        :param senDF:           DataFrame of sentences with args_id's to identify arguments
        :param args_column:     Column of the meta data
        :return:                Dataframe of sentences with a column containing the meta data
        """
        # Add "arg_" to column name for senDF
        sen_column = "arg_" + args_column

        # Create a new column in senDF
        senDF.loc[:, sen_column] = pd.Series(dtype="float64")

        # Update the values in senDF
        for i, row in argsDF.iterrows():
            argID = row['docno']
            value = row[args_column]

            senDF.loc[senDF['args_id'] == argID, sen_column] = value

        return senDF


    # Function that adds new retrieval results to the file in outpath (In the TREC format)
    def writeTRECLine(self, query_num, method, outpath, row, senAnalyzer):
        """
        :param query_num:       Number of the query in a queryset
        :param method:          Description of the method used in retrieval
        :param outpath:         Path to the csv-file for the results to be written
        :param row:             Row of a DF that should be written to a CSV
        :param senAnalyzer:     Sentiment Analyzer to score the query and discussion title (potentially update stance)
        """
        # Get the information to be written into the file
        title = row["arg_discussion_title"]
        stance = row["arg_stance"]
        fst_sen = row["docno"]
        snd_sen = row["pair_id"]
        pair = fst_sen + "," + snd_sen
        rank = row['rank'] + 1
        score = row["finalScore"]
        tag = "Pearl" + method

        # Update the stance to the query based on the sentiment analysis
        stance = senAnalyzer.updateStanceBySentiment(discussion_title=title,
                                                     arg_stance=stance)

        # Create the new line and write it to the file (Append mode to only add new lines)
        new_line = str(query_num) + ' ' + stance + ' ' + pair + ' ' + str(rank) + ' ' + str(score) + ' ' + tag + '\n'
        with open(outpath, "a") as outf:
            outf.write(new_line)
            outf.close()

    # Function that adds new retrieval results to the file in outpath
    def writeLine(self, query_num, outpath, query, row, senAnalyzer):
        """
        :param query_num        Number of the query in a queryset
        :param outpath:         Path to the csv-file for the results to be written
        :param query:           String of the query that was used to get the results
        :param row:             Row of a DF that should be written to a CSV
        :param senAnalyzer:     Sentiment Analyzer to score the query and discussion title (potentially update stance)
        """
        # Get the information to be written into the file
        sentence = row["sentence"]
        partner = row["pair_sen"]
        sen_id = row["docno"]
        arg_score = row["arg_score"]
        sen_score = row["score"]
        pair_id = row["pair_id"]
        stance = row["arg_stance"]
        argument = row["argument"]
        title = row["arg_discussion_title"]

        # Create the pair (and check for point at the end of the sentence)
        if (sentence[-1] != '.'):
            pair = sentence + ". " + partner
        else:
            pair = sentence + " " + partner

        # Update the stance to the query based on the sentiment analysis
        stance_to_query = senAnalyzer.updateStanceBySentiment(discussion_title=title,
                                                              arg_stance=stance)

        # Create the new line and write it to the file (Append mode to only add new lines)
        new_line = str(query_num) + ',"' + query + '",' + sen_id + ',' + str(arg_score) + ',' + str(sen_score) + ',' + \
                   pair_id + ',"' + pair + '","' + argument + '",' + stance + ',"' + title + '",' + stance_to_query + '\n'
        with open(outpath, "a") as outf:
            outf.write(new_line)
            outf.close()

    # Function to get the last query that was processed
    def getLastQuery(self, filepath):
        df = pd.read_csv(filepath)
        return df["query"].iloc[-1]





class PearlIndexer():
    def __init__(self, argsInPath, argsOutPath, senPath, mode=0):

        """

        :param argsInPath:  Path to the unprocessed argument CSV
        :param argsOutPath: Path to the processed argument CSV
        :param senPath:     Path to the processed sentences CSV
        :param mode:        0=ArgsIndexing, 1=SenIndexing, Else: Other functions
        """

        # Path variables
        self.argsInPath = argsInPath
        self.argsOutPath = argsOutPath
        self.senPath = senPath


        # Prepare the indexers
        if mode==0:
            self.argsIndexer = ind.ArgsIndexer(filepath=self.argsOutPath)

        if mode==1:
            self.senIndexer = ind.SentencesIndexer(filepath=self.senPath)



    # CSV split
    def argsSplit(self):
        split.args_split(self.argsInPath, self.argsOutPath)

    def sentencesSplit(self):
        split.sentence_split(self.argsOutPath, self.senPath)



    # Indexing
    def indexArgs(self):
        if not hasattr(self, "argsIndexer"):
            self.argsIndexer = ind.ArgsIndexer(filepath=self.argsOutPath)

        self.argsIndexer.indexArgsDF()

    def indexSentences(self):
        if not hasattr(self, "senIndexer"):
            self.senIndexer = ind.SentencesIndexer(filepath=self.senPath)

        self.senIndexer.indexSenDF()

    # Add columns
    def addColumnsToArgs(self, quality_dir):
        if not hasattr(self, "argsIndexer"):
            self.argsIndexer = ind.ArgsIndexer(filepath=self.argsOutPath)

        self.argsIndexer.addQualityColumn(quality_dir)
