import pyterrier as pt
import string
import pandas as pd
import os

# Don't show warnings about chained assignments
pd.options.mode.chained_assignment = None

# Provide path for JAVA
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-11-openjdk-amd64"
if not pt.started():
    pt.init()


class ArgsRetriever:
    """
    Retriever class to obtain arguments based on a query and a precalculated index
    """

    def __init__(self, arg_path,
                 index_path="./Indexing/index/data.properties",
                 modeltype="DirichletLM",
                 num_results=1000):
        """
        :param index_path: Path to the PyTerrier Index files
        :param modeltype: Retrieval model to be used
        :param arg_path: Path to the CSV-file containing all arguments
        :param num_results: Number of results to be obtained per query
        """

        self.index = pt.IndexFactory.of(index_path)

        # Define the retriever: Where to find the index, the model, the number of results and the metadata
        self.model = pt.BatchRetrieve(self.index,
                                      wmodel=modeltype,
                                      num_results=num_results,
                                      metadata=["docno", "stance", "discussion_title", "quality"])
        self.arg_path = arg_path

        # Arguments can be read in
        self.readArgsCSV()
        print("- Argument-Retriever initialized with the %s retrieval model" %modeltype)



    def readArgsCSV(self, chunksize=10000):
        """
        Function that reads in the argument body to be used in retrieval
        :param chunksize: The number of lines to be read in from the CSV at once
        """
        self.argDF = pd.DataFrame


        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.arg_path, chunksize=chunksize)
        for i, chunk in enumerate(rows):
            # Preprocess the chunk
            chunk = self.preprocessDF(df=chunk)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.argDF = chunk
            else:
                self.argDF = pd.concat([self.argDF, chunk], axis=0)



    def retrieve(self, query):
        """
        Function to find relevant arguments to a query
        :param query: String that will be used for the retrieval
        """
        # Remove punctation
        query = query.translate(str.maketrans('', '', string.punctuation))

        # Obtain the relevant arguments with PyTerrier
        # self.retrievalDF = self.model.search(query)

        # Query Expansion using Bo1 Divergence
        bo1 = pt.rewrite.Bo1QueryExpansion(self.index)
        bo1_pipe = self.model >> bo1 >> self.model
        # Obtain the relevant arguments with PyTerrier using query expansion
        self.retrievalDF = bo1_pipe.search(query)

        # Change the type of the quality column to float
        self.retrievalDF['quality'] = self.retrievalDF['quality'].astype(float)



    def matchArgStringsToDocnoDF(self, df):
        """
        Function to obtain arguments that fit the docno (args_id) of the given DataFrame of sentences
        """
        # Create an empty column in the provided DF
        df.loc[:, 'argument'] = pd.Series(dtype="float64")

        # Get the rows from the argsDF that match the id's in df
        stringDF = self.argDF.loc[self.argDF['docno'].isin(df['args_id'])]

        # Update the values in df
        for i, row in stringDF.iterrows():
            docno = row['docno']
            argument = row['premises'] + ' Conclusion: ' + row['conclusion']
            df.loc[df['args_id'] == docno, 'argument'] = argument

        return df



    # Function to preprocess a Dataframe
    def preprocessDF(self, df):
        """
        Function to drop columns that are unnecessary for retrieval
        :param df: Dataframe to be altered for later processing
        :return: Updated Dataframe
        """

        # Rename the id column to docno to follow PyTerrier's naming convention
        df.rename(columns={'id': 'docno'}, inplace=True)

        # Drop unnecessary columns
        dropcolums = ["quality"]
        for column in dropcolums:
            df.drop(column, axis=1, inplace=True)

        return df





class SentencesRetriever:
    """
    Retriever class to obtain sentences based on a query and a precalculated index
    """

    def __init__(self, sen_path,
                 index_path="./Indexing/sentences_index/data.properties",
                 modeltype="DPH",
                 num_results=5000):
        """
        :param index_path: Path to the PyTerrier Index files
        :param modeltype: Retrieval model to be used
        :param sent_path: Path to the CSV-file containing all sentences
        :param num_results: Number of results to be obtained per query
        """

        self.index = pt.IndexFactory.of(index_path)

        # Define the retriever: Where to find the index, the model, the number of results and the metadata
        self.model = pt.BatchRetrieve(self.index,
                                      wmodel=modeltype,
                                      num_results=num_results,
                                      metadata=["args_id", "docno", "sent_quality",
                                                "partner", "partner_position", "partner_quality"])
        self.sen_path = sen_path

        self.readSenCSV()
        print("- Sentences-Retriever initialized with the %s retrieval model" %modeltype)





    def readSenCSV(self, chunksize=10000):
        """
        Function that reads in the sentences body to be used in retrieval
        :param chunksize: The number of lines to be read in from the CSV at once
        """
        self.senDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.sen_path, chunksize=chunksize)
        for i, chunk in enumerate(rows):
            # Preprocess the chunk
            chunk = self.preprocessDF(chunk)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.senDF = chunk
            else:
                self.senDF = pd.concat([self.senDF, chunk], axis=0)



    def retrieve(self, query):
        """
        Function to find relevant sentences to a query
        :param query: String that will be used for the retrieval
        """
        # Remove punctation
        query = query.translate(str.maketrans('', '', string.punctuation))

        # Obtain the relevant sentences with PyTerrier
        # self.retrievalDF = self.model.search(query)

        # Query Expansion using Bo1 Divergence
        bo1 = pt.rewrite.Bo1QueryExpansion(self.index)
        bo1_pipe = self.model >> bo1 >> self.model
        # Obtain the relevant arguments with PyTerrier using query expansion
        self.retrievalDF = bo1_pipe.search(query)

        # Change the type of the quality columns to float
        self.retrievalDF['sent_quality'] = self.retrievalDF['sent_quality'].astype(float)
        self.retrievalDF['partner_quality'] = self.retrievalDF['partner_quality'].astype(float)





    def matchSenStringsToDocnoDF(self, df):
        """
        Function to obtain text passages to a given DataFrame of sentences
        """
        # Create empty column in the provided DF
        df.loc[:, 'sentence'] = pd.Series(dtype="float64")

        # Get the rows from the senDF that match the id's in df
        stringDF = self.senDF.loc[self.senDF['docno'].isin(df['docno'])]

        # Update the values in df
        for i, row in stringDF.iterrows():
            docno = row['docno']
            df.loc[df['docno'] == docno, 'sentence'] = row['sentence']

        return df



    # Function to preprocess a Dataframe
    def preprocessDF(self, df):
        """
        Function to drop columns that are unnecessary for retrieval
        :param df: Dataframe to be altered for later processing
        :return: Updated Dataframe
        """

        # Rename the id column to docno to follow PyTerrier's naming convention
        df.rename(columns={'sen_id': 'docno'}, inplace=True)

        # If the sentences csv is not the slim version, drop unnecessary columns (quality refers to argument quality)
        if not "slim" in self.sen_path:
            dropcolums = ["arg_ids", "stance", "quality", "sent_quality", "partner", "partner_position", "partner_quality"]
            for column in dropcolums:
                df.drop(column, axis=1, inplace=True)

        return df