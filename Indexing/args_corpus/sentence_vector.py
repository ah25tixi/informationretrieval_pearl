import pandas as pd
from sentence_transformers import SentenceTransformer

df = pd.read_csv('sentences.csv')
dict0 = pd.Series(df.sentence.values, index=df.sen_id).to_dict()
lst = list(dict0.values())

model = SentenceTransformer('bert-base-nli-mean-tokens')

final = {}
counter = 0
for key in dict0:
    final[key] = model.encode(dict0[key])
    counter += 1
    print(f"{counter} / {len(lst)} = {counter/len(lst)*100}")

out = pd.DataFrame.from_dict(final, orient='index')
out.to_csv('sentence_vector.csv')
