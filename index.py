import pyterrier as pt
import pandas as pd
import os

# Provide path for JAVA and initialize pyterrier
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-11-openjdk-amd64"
if not pt.started():
    pt.init()


class ArgsIndexer:
    def __init__(self, filepath, chunksize=10000, indexpath="./Indexing/index"):
        """
        :param filepath: The path to the CSV to be processed
        :param chunksize: The number of lines to be read in from the CSV at once
        :param indexpath: The path for the index to be saved at
        """

        self.filepath = filepath
        self.chunksize = chunksize
        self.indexpath = indexpath

        # Load the Dataframe
        self.loadArgsCSV()

    # Function to preprocess the args Dataframe
    def preprocessArgsDF(self, df):
        """
        Function to prepare the dataframe
        - Rename columns to fit PyTerrier
        - Prepare joint column for the document text

        :param df: Dataframe to be altered for later processing
        :return: Updated Dataframe
        """
        # Rename the id column to docno to follow PyTerrier's naming convention
        df.rename(columns={'id': 'docno'}, inplace=True)

        # Concatenate the conclusion and premises column
        df['text'] = df['premises'] + ". " + df['conclusion']

        # Drop merged columns
        dropcolums = ["conclusion", "premises"]
        for column in dropcolums:
            df.drop(column, axis=1, inplace=True)

        return df

    # Function to load the args-CSV as a Dataframe
    def loadArgsCSV(self):
        """
        Function that sets the argsDF attribute
        """

        self.argsDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.filepath, chunksize=self.chunksize)
        for i, chunk in enumerate(rows):
            # Preprocess the chunk
            chunk = self.preprocessArgsDF(chunk)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.argsDF = chunk
            else:
                self.argsDF = pd.concat([self.argsDF, chunk], axis=0)

            print("Read %d arguments" % len(self.argsDF))

        # Change the type of the quality column to string to use it as metadata
        self.argsDF["quality"] = self.argsDF["quality"].astype(str)

        print("Successfully loaded CSV from %s as Dataframe.\n" % self.filepath)

    # Function to index the provided Dataframe
    def indexArgsDF(self):
        """
        Function to apply PyTerrier's indexing
        """

        print("\nIndexing the arguments...")

        # Create an indexer and index the dataset (Text is the document body and other columns are saved as meta data)
        # - overwrite=True: Existing index is overwritten
        # - verbose=True:    The indexing progress is shown on the console
        pd_indexer = pt.DFIndexer(self.indexpath, overwrite=True, verbose=True, blocks=True)
        indexref = pd_indexer.index(self.argsDF["text"],
                                    self.argsDF[["docno", "stance", "discussion_title", "quality"]])

        # Print information on the index
        index = pt.IndexFactory.of(indexref)
        print("\n=====================================")
        print("     Index statistics")
        print("=====================================")
        print(index.getCollectionStatistics().toString())

    # Function to add a quality column to the args CSV
    def addQualityColumn(self, quality_dir):
        # Create an empty DF to store the quality values
        qualityDF = pd.DataFrame()

        # Iterate over all quality files
        print("\n=================================")
        print("Adding quality column to arguments")
        print("=================================")
        for i, _ in enumerate(os.listdir(quality_dir)):
            filename = quality_dir + "/args_processed_part_" + str(i + 1) + "_quality_no_premise.csv"
            print("Reading in file %i" % (i + 1))

            # Read in the file as DF
            chunk = pd.read_csv(filename)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                qualityDF = chunk
            else:
                qualityDF = pd.concat([qualityDF, chunk], axis=0)

        # After reading in the entire quality values, add them to the argsDF
        print("Merging QualityDF into ArgsDF")
        self.argsDF['quality'] = pd.Series()

        # Set the id's for matching
        qualityDF.set_index('id', inplace=True)
        self.argsDF.set_index('id', inplace=True)

        # Get the updated quality column and merge it into the argsDF
        updatedColumn = self.argsDF['quality'].combine_first(qualityDF['quality'])
        self.argsDF['quality'] = updatedColumn

        # Write the argsDF to a CSV
        new_filename = self.filepath.replace('.csv', '_quality.csv')
        self.argsDF.to_csv(new_filename)

    def preprocessArgsDFForQuality(self, df):
        # Drop unnecessary columns
        dropcolums = ["context1", "context2"]
        for column in dropcolums:
            df.drop(column, axis=1, inplace=True)

        return df


class SentencesIndexer:
    def __init__(self, filepath="./Indexing/args_corpus/sentences.csv",
                 chunksize=10000,
                 indexpath="./Indexing/sentences_index",
                 pairpath="./Indexing/args_corpus/qualtest.csv",
                 pairout="./Indexing/args_corpus/qualtest_out_2.csv"):
        """
        :param filepath: The path to the CSV to be processed
        :param chunksize: The number of lines to be read in from the CSV at once
        :param indexpath: The path for the index to be saved at
        """

        self.filepath = filepath
        self.chunksize = chunksize
        self.indexpath = indexpath
        self.pairpath = pairpath
        self.pairOutpath = pairout

        # Load the Dataframe
        self.loadSenCSV()
        # self.createOptimalPairs()

    # Function to load the Sentences-CSV as a Dataframe
    def loadSenCSV(self):
        """
        Function that sets the senDF attribute
        """

        self.senDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.filepath, chunksize=self.chunksize)
        for i, chunk in enumerate(rows):
            # Rename the sen_id column
            chunk.rename(columns={'sen_id': 'docno'}, inplace=True)
            chunk.rename(columns={'arg_ids': 'args_id'}, inplace=True)

            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.senDF = chunk
            else:
                self.senDF = pd.concat([self.senDF, chunk], axis=0)

            print("Read %d sentences" % len(self.senDF))

        # Change the type of the quality columns to string to use it them as metadata
        self.senDF["sent_quality"] = self.senDF["sent_quality"].astype(str)
        self.senDF["partner_quality"] = self.senDF["partner_quality"].astype(str)

        print("Successfully loaded CSV from %s as Dataframe.\n" % self.filepath)

    # Function to index the provided Dataframe
    def indexSenDF(self):
        """
        Function to apply PyTerrier's indexing
        """

        print("\nIndexing the sentences...")

        # Create an indexer and index the dataset
        # - overwrite=True: Existing index is overwritten
        # - verbose=True:    The indexing progress is shown on the console
        pd_indexer = pt.DFIndexer(self.indexpath, overwrite=True, verbose=True, blocks=True)
        # First parameter is the text body, second parameter contains the meta data
        indexref = pd_indexer.index(self.senDF["sentence"], self.senDF[["args_id",
                                                                        "docno",
                                                                        "sent_quality",
                                                                        "partner",
                                                                        "partner_position",
                                                                        "partner_quality"]])

        # Print information on the index
        index = pt.IndexFactory.of(indexref)
        print("\n=====================================")
        print("     Index statistics")
        print("=====================================")
        print(index.getCollectionStatistics().toString())

    # Function that takes a CSV of quality scores for pairs and saves the optimal pair
    # - The CSV contains every sentence with a quality score for matching with the preceding and succeeding sentence
    # - The optimal pair out of the two options is written to a new CSV-file with positional information
    def createOptimalPairs(self):
        if not hasattr(self, "pairDF"):
            self.loadPairQuality()

        length = len(self.pairDF)
        new_content = True

        # Create a file with header (if non-existent)
        if os.path.exists(self.pairOutpath):
            new_content = False
        else:
            with open(self.pairOutpath, "w") as outf:
                # Write header
                outf.write("sen_id,partner,quality,position\n")
                outf.close()

        # If a file was found, get the last processed sentence
        last_id = ""
        if not new_content:
            last_id = self.getLastID(self.pairOutpath)

        # Initialize loop variables
        id1 = ""
        id2 = ""
        quality = 0

        # Loop over the premises and find the optimal pair (Highest quality)
        for i, row in self.pairDF.iterrows():
            # Give status update
            print("Processing row {}/{}".format(i, length))

            newID1 = row["sen_id_1"]

            # Check if the current id is the last that was processed to set new_content to true
            # If not, pass this sentence
            if not new_content:
                if newID1 == last_id:
                    new_content = True
                    print("-" * 50)
                    print("Starting to write new pairs")
                    print("-" * 50)
                continue

            # Get necessary information
            newQuality = row["sent_pair_quality"]
            newID2 = row["sen_id_2"]

            # Write pairs based on the following conditions:
            # - Repeating ID:               Choose the higher quality
            # - New ID:                     New argument started, hence pair is formed between 1st and 2nd sentence
            # - Sentence is conclusion:     Write in a flipped way
            if newQuality < quality:
                if newID1 == id2:
                    self.writePair(sen_id=newID1, qual=quality, partner=id1, pos="before")
            else:
                self.writePair(sen_id=newID1, qual=newQuality, partner=newID2, pos="after")

            if 'CONC' in newID2:
                self.writePair(sen_id=newID2, qual=newQuality, partner=newID1, pos="before")

            # Update variables for the next iteration
            id1 = newID1
            id2 = newID2
            quality = newQuality

    def writePair(self, sen_id, qual, partner, pos):
        with open(self.pairOutpath, "a") as outf:
            outf.write(sen_id + "," + partner + "," + str(qual) + "," + pos + "\n")
            outf.close()

    def loadPairQuality(self):
        self.pairDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(self.pairpath, chunksize=self.chunksize)
        for i, chunk in enumerate(rows):
            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.pairDF = chunk
            else:
                self.pairDF = pd.concat([self.pairDF, chunk], axis=0)

            print("Read %d pairs" % len(self.pairDF))

        print("Successfully loaded CSV from %s as Dataframe.\n" % self.filepath)

    # Function to get the id of the last sentence that was processed
    def getLastID(self, filepath):
        df = pd.read_csv(filepath)
        return df["sen_id"].iloc[-1]




    # Function that takes the CSV created by createOptimalPairs and appends it to the sentencesDF
    def appendPairColumns(self, pairpath, outpath):
        if not hasattr(self, "senDF"):
            self.loadSenCSV()

        # Load the optimal pairs
        self.loadOptimalPairs(pairpath)

        # Create new columns in senDF
        self.senDF["partner"] = "No partner"
        self.senDF["partner_position"] = "after"
        self.senDF["partner_quality"] = 0

        # Use map to match the quality columns to the senDF
        print("Mapping the columns of the optimal pairs...")
        self.senDF.partner = self.senDF.docno.map(self.optimalPairDF.set_index('sen_id').partner)
        print("- Partner ID mapped")
        self.senDF.partner_position = self.senDF.docno.map(self.optimalPairDF.set_index('sen_id').position)
        print("- Partner Position mapped")
        self.senDF.partner_quality = self.senDF.docno.map(self.optimalPairDF.set_index('sen_id').quality)
        print("- Partner Quality mapped")

        # Fill na-Values
        self.senDF["partner"] = self.senDF["partner"].fillna(value="No partner")
        self.senDF["partner_position"] = self.senDF["partner_position"].fillna(value="after")
        self.senDF["partner_quality"] = self.senDF["partner_quality"].fillna(value=0)

        # Write the result to a new file (Rename back to fit naming convention)
        self.senDF.rename(columns={'docno': 'sen_id'}, inplace=True)
        self.senDF.rename(columns={'args_id': 'arg_ids'}, inplace=True)

        header = ["arg_ids", "sen_id", "sentence", "conclusion", "stance", "quality", "sent_quality", "partner",
                  "partner_position", "partner_quality"]
        self.senDF.to_csv(outpath, columns=header)
        print("Updated file sucessfully written to " + outpath)


    def loadOptimalPairs(self, filepath):
        self.optimalPairDF = pd.DataFrame

        # Read the file into a Dataframe (In chunksize chunks)
        rows = pd.read_csv(filepath, chunksize=self.chunksize)
        for i, chunk in enumerate(rows):
            # Either initialize df with chunk or add the chunk to the existing df
            if i == 0:
                self.optimalPairDF = chunk
            else:
                # Ensure that no duplicate ids are added
                self.optimalPairDF = pd.concat([self.optimalPairDF, chunk], axis=0).drop_duplicates(subset=["sen_id"])

            print("Read %d pairs" % len(self.optimalPairDF))

        print("Successfully loaded CSV from %s as Dataframe.\n" % filepath)
